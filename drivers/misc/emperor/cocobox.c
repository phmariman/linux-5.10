/*
 *  Driver for supporting the radio on the COCO connectivity box.
 *
 *  The radio can be controlled with a reset gpio line.
 *  If the Kinetis MCU gets a reset, it jumps for 2 seconds in a bootloader,
 *  	where it can download a firmware.
 *  The MCU pulls a 'status' gpio pin high, if it is application mode.
 *
 *  Radio is set to a specific protocol (easywave, easywavesharp, transparent).
 *
 *  Device-tree setup; add a node in the soc device
 *	---------------
 *	soc {
 *		cocobox {
 *			compatible = "cocobox";
 *		};
 *	};
 *
 *  sysfs interface
 *	---------------
 *	A directory is created /sys/kernel/emperor/cocobox and /sys/kernel/emperor/radio1
 *	Following files are created (besides the normal i2c slave files):
 *		bootloader (wo)		echo 1 for a soft reboot (saving critical data); 2 for a hard reboot
 *		protocol (rw)		show/store the active protocol
 *		radiostate (rw)		state of the radio; touch file to re-probe the radio
 *		swversion (ro)		shows the firmware version
 *		rssi (ro)			rssi value
 *		lqi (ro)			link quality indicator
 *		led (wo)			led on/off (1/0)
 *		mode (ro)			2 = initialized, 1 = probed, 0 = unprobed
 *		capabilities (ro)	shows which RF protocols the firmware supports (hex value)
 *
 *	udev interface
 *	---------------
 *	If the protocol has changed, a message is sent to udev.
 *	Following udev environment variables are set:
 *		PROTOCOL	active protocol
 *		STATE		state of the radio
 *	Match rules on DRIVER=="cocobox"
 *	udev script sets the device link to the connected uart depending the protocol
 *		/dev/easywave /dev/easywavesharp /dev/transparent1 /dev/bootloader1
 *
 *	eeprom interface
 *	---------------
 *	MAC address is read from eeprom, transformed to easywave sharp MAC address and programmed in MCU
 */

#include <linux/init.h>
#include <linux/kobject.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/pm_runtime.h>
#include <linux/gpio/consumer.h>
#include <linux/i2c-algo-bit.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <asm/io.h>
#include <linux/io.h>
#include <linux/tty.h>
#include <linux/usb.h>
#include <linux/usb/serial.h>
#include <linux/regmap.h>
#include <linux/nvmem-consumer.h>
#include <linux/random.h>
#include <linux/workqueue.h>
#include <linux/regulator/consumer.h>

#include "emperor-radio.h"

#define DRIVER_NAME	"cocobox"
#define RADIONAME	"radio1"
#define COMMON_VID		0x10C4
#define ZB_BRICK_PID	0x7654
#define P1_PID			0x7655
#define EW_STICK_PID	0x7656

// CP210x requires some time to init before this driver can probe the device
#define CP210x_DELAY_DRIVER 200 // time in msec
// the 868MHz radio MCU first starts a bootloader; wait till it jumps into application
#define DELAY_868_MCU 2000 // time in msec

#define CP210x_RETRY_COUNT 10
// maximum time to flash the MCU
#define RADIO_POLL_PERIOD 30

extern struct kobject *emperor_kobj;

typedef enum {
	STATE_UNPROBED = 0,
	STATE_PROBED,
	STATE_INITIALIZED,
} I2C_STATE;

typedef enum {
	STATE_NORADIO = S_NORADIO,
	STATE_BOOTLOADER = S_BOOTLOADER,
	STATE_APPLICATION = S_OPERATIONAL,
} MCU_STATE;

struct cocobox_data {
	struct device *dev;
	I2C_STATE i2c_state;
	struct gpio_desc *sda;
	struct gpio_desc *scl;
	struct gpio_desc *resetpin;
	struct gpio_desc *statepin;
	struct gpio_desc *led;
	struct i2c_adapter adap;
	struct i2c_algo_bit_data bit_data;
	struct i2c_client *client;
	struct regmap *regmap;
	PROTOCOLS protocol; // Selected protocol
	MCU_STATE mcu_state; // State of the radio
	int swversion; // sw version
	struct delayed_work p1_work;
	struct delayed_work ew_work;
	int p1_work_cnt;
	int ew_work_cnt;
	struct regulator *reg_usbh1;
	struct regulator *reg_usbotg;
	struct delayed_work dwork;	// Worker for probing the MCU
};

struct usb_ids {
	u16 vid;
	u16 pid;
};

struct usbport_trig_data {
	struct device *dev;
	struct notifier_block nb;
};
struct usbport_trig_data *usbport_data;

struct i2c_board_info i2c_easywave_radio = {
	I2C_BOARD_INFO("easywave_radio", RADIO2_ADDR),
};

static void clear_data(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	data->i2c_state = STATE_UNPROBED;
	data->sda = NULL;
	data->scl = NULL;
	data->resetpin = NULL;
	data->statepin = NULL;
	data->client = NULL;
	data->regmap = NULL;
	data->protocol = P_UNSUPPORTED;
	data->mcu_state = STATE_NORADIO;
	data->swversion = 0;
	data->ew_work_cnt = 0;
	data->p1_work_cnt = 0;
}

static void i2c_gpio_setsda_val(void *data, int state)
{
	struct cocobox_data *priv = data;
	gpiod_set_value_cansleep(priv->sda, state);
}

static void i2c_gpio_setscl_val(void *data, int state)
{
	struct cocobox_data *priv = data;
	gpiod_set_value_cansleep(priv->scl, state);
}

static int i2c_gpio_getsda(void *data)
{
	struct cocobox_data *priv = data;
	return gpiod_get_value_cansleep(priv->sda);
}

static int i2c_gpio_getscl(void *data)
{
	struct cocobox_data *priv = data;
	return gpiod_get_value_cansleep(priv->scl);
}

static void write_ews_mac(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	char macwan[MAC_SIZE+1]; // MAC address gateway
	char mac_ews[EWSMAC_SIZE+1]; // EW# MAC address
	long int ewsaddress = 0;
	u8 buf_r[EWSMAC_SIZE/2] = {0};
	u8 buf_w[EWSMAC_SIZE/2] = {0};
	char *value;
	size_t cell_len;
	struct nvmem_cell *cell;

	if (regmap_bulk_read(data->regmap, RADIO_EWS_MAC0, buf_r, (EWSMAC_SIZE/2)))
		dev_err(dev, "Unable to read EW# MAC address\n");

	cell = nvmem_cell_get(dev, "macwan");
	if (IS_ERR(cell)) {
		dev_err(dev, "failed to get nvmem cell macwan\n");
		return;
	}

	value = nvmem_cell_read(cell, &cell_len);
	nvmem_cell_put(cell);
	if (IS_ERR(value)) {
		return;
	}

	snprintf(macwan, (MAC_SIZE+1), "%s", value);
	kfree(value);

	strncpy(mac_ews, EWSBANK, 2);
	strncpy(mac_ews+2, macwan+8, 4);
	mac_ews[EWSMAC_SIZE] = '\0';

	if (kstrtol(mac_ews, 16, &ewsaddress)) {
		dev_err(dev, "Unable to create EW# MAC address\n");
		return;
	}

	buf_w[2] = (int)ewsaddress & 0xFF;
	buf_w[1] = ((int)ewsaddress & 0xFF00) >> 8;
	buf_w[0] = ((int)ewsaddress & 0xFF0000) >> 16;

	if ((buf_w[0] == buf_r[0]) && (buf_w[1] == buf_r[1]) && (buf_w[2] == buf_r[2])) {
		dev_info(dev, "EW# MAC already written: :%06lX\n", ewsaddress);
		return;
	}

	if (regmap_bulk_write(data->regmap, RADIO_EWS_MAC0, buf_w, (EWSMAC_SIZE/2)))
		dev_err(dev, "unable to write EW# MAC address\n");
	else
		dev_info(dev, "EW# MAC address written: :%06lX\n", ewsaddress);
}

static void talk_to_udev(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	char env_protocol[DESC_SIZE+10];
	char env_state[DESC_SIZE+10];
	char *envp[] = { env_protocol, env_state, NULL };

	sprintf(env_protocol, "PROTOCOL=%s", protocol_description[data->protocol]);
	sprintf(env_state, "STATE=%s", state_description[data->mcu_state]);
	kobject_uevent_env(&dev->kobj, KOBJ_CHANGE, envp);
}

static void protocol_settings(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	// baudrate
	if (data->protocol == P_EASYWAVE)
		regmap_write(data->regmap, RADIO_UART, R13_UART57K6);
	else
		regmap_write(data->regmap, RADIO_UART, R13_UART115K2);

	// mac setting
	if (data->protocol == P_EASYWAVESHARP)
		write_ews_mac(dev);
}

static void init_radio(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	int lifetest = 0;
	int statepin = 0;
	unsigned int val = 0;
	u8 i;

	if (data->i2c_state != STATE_INITIALIZED) {
		dev_warn(dev, "cannot init radio because cocobox is not initialized\n");
		return;
	}

	statepin = gpiod_get_value(data->statepin);
	// statepin should be pull-downed, which cannot be done with cp2102n -> resistor to be added
	get_random_bytes(&i, sizeof(i));

	if (!regmap_write(data->regmap, RADIO_SCRATCH, i)) {
		regmap_read(data->regmap, RADIO_SCRATCH, &val);
		if (val == i)
			lifetest = 1;
	}

	dev_info(dev, "radio statepin:%d, lifetest:%d\n", statepin, lifetest);

	if (lifetest)
		data->mcu_state = STATE_APPLICATION;
	else {
		data->mcu_state = STATE_BOOTLOADER;
		dev_err(dev, "radio not operational\n");
		goto udev_message;
	}

	regmap_read(data->regmap, RADIO_FWVERSION, &data->swversion);
	regmap_read(data->regmap, RADIO_ACTIVE_PROT, &val);
	if (val & R2B3_EASYWAVE)
		data->protocol = P_EASYWAVE;
	else if (val & R2B5_EASYWAVESHARP)
		data->protocol = P_EASYWAVESHARP;
	else if (val & R2B0_TRANSPARENT)
		data->protocol = P_TRANSPARENT;
	else
		data->protocol = P_UNSUPPORTED;

	protocol_settings(dev);

udev_message:
	talk_to_udev(dev);
	dev_info(dev, "radio state=%s protocol=%s\n", state_description[data->mcu_state], protocol_description[data->protocol]);
}

static void poll_radio(struct work_struct *work)
{
	struct cocobox_data *data = container_of(work, struct cocobox_data, dwork.work);

	init_radio(data->dev);
}

static void deinit_868stick(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	cancel_delayed_work_sync(&data->dwork);
	i2c_unregister_device(data->client);
	i2c_del_adapter(&data->adap);
	// let udev remove the protocol link
	data->protocol = P_UNSUPPORTED;
	data->mcu_state = STATE_NORADIO;
	talk_to_udev(dev);
	dev_info(dev, "deinit 868MHz stick\n");

	return;
}

static int usb_descriptor_check(struct usb_device_descriptor descriptor, u16 vid, u16 pid)
{
	u16 usb_vid = le16_to_cpu(descriptor.idVendor);
	u16 usb_pid = le16_to_cpu(descriptor.idProduct);

	if ((usb_vid == vid) && (usb_pid == pid))
		return 1;

	return 0;
}

static int gpiochip_product_match(struct gpio_chip *chip, void *product_match)
{
	struct usb_serial *serial = gpiochip_get_data(chip);
	struct usb_ids *match = product_match;

	if (serial == NULL)
		return 0;

	return usb_descriptor_check(serial->dev->descriptor, match->vid, match->pid);
}

static void init_868stick(struct work_struct *work)
{
	struct cocobox_data *data = container_of(work, struct cocobox_data, ew_work.work);
	struct gpio_chip *chip;
	struct usb_serial *serial = NULL;
	struct i2c_algo_bit_data *bit_data;
	struct i2c_adapter *adap;
	int gpio_base = 0;
	struct usb_ids match;

	match.vid = COMMON_VID;
	match.pid = EW_STICK_PID;

	if (data->i2c_state > STATE_PROBED) {
		dev_err(data->dev, "init already in progress\n");
		return;
	}

	data->i2c_state = STATE_PROBED;

	chip = gpiochip_find(&match, gpiochip_product_match);
	if (chip == NULL) {
		data->i2c_state = STATE_UNPROBED;
		data->ew_work_cnt++;
		if (data->ew_work_cnt < CP210x_RETRY_COUNT) {
			dev_dbg(data->dev, "didn't find 868MHz stick, retry %d\n", data->ew_work_cnt);
			schedule_delayed_work(&data->ew_work, msecs_to_jiffies(CP210x_DELAY_DRIVER));
		} else {
			dev_err(data->dev, "didn't find 868MHz stick, given up\n");
		}
		return;
	}

	serial = gpiochip_get_data(chip);
	gpio_base = chip->base;
	dev_info(data->dev, "868MHz radio probed (%d): '%s' '%s' (gpiochip%d)\n", data->ew_work_cnt+1, serial->dev->product,
			serial->dev->manufacturer, gpio_base);

	data->scl = gpio_to_desc(gpio_base);
	data->sda = gpio_to_desc(gpio_base+1);
	data->resetpin = gpio_to_desc(gpio_base+2);
	data->statepin = gpio_to_desc(gpio_base+3);
	if (data->scl == NULL || data->sda == NULL || data->resetpin == NULL || data->statepin == NULL) {
		dev_err(data->dev, "couldn't map gpio number\n");
		data->i2c_state = STATE_UNPROBED;
		return;
	}

	// bring MCU out of reset
	gpiod_set_value(data->resetpin, 1);

	adap = &data->adap;
	bit_data = &data->bit_data;

	bit_data->can_do_atomic = true;
	bit_data->setsda = i2c_gpio_setsda_val;
	bit_data->setscl = i2c_gpio_setscl_val;
	bit_data->getscl = i2c_gpio_getscl;
	bit_data->getsda = i2c_gpio_getsda;
	bit_data->udelay = 1000; // 200Hz measured frequency
	bit_data->timeout = HZ;
	bit_data->data = data;

	snprintf(adap->name, sizeof(adap->name), "i2c-cocobox");
	adap->owner = THIS_MODULE;
	adap->algo_data = bit_data;
	adap->dev.parent = data->dev;
	adap->nr = -1;
	if (i2c_bit_add_bus(adap)) {
		dev_err(data->dev, "error creating i2c adapter\n");
		data->i2c_state = STATE_UNPROBED;
		return;
	}

	data->client = i2c_new_client_device(&data->adap, &i2c_easywave_radio);
	if (IS_ERR(data->client)) {
		dev_err(data->dev, "failed to allocate I2C device\n");
		i2c_del_adapter(&data->adap);
		data->i2c_state = STATE_UNPROBED;
		return;
	}

	data->regmap = devm_regmap_init_i2c(data->client, &radio_regmap_config);
	if (IS_ERR(data->regmap)) {
		dev_err(data->dev, "regmap init failed\n");
		i2c_unregister_device(data->client);
		i2c_del_adapter(&data->adap);
		data->i2c_state = STATE_UNPROBED;
		return;
	}

	data->i2c_state = STATE_INITIALIZED;
	dev_info(data->dev, "I2C over GPIO interface created\n");
	init_radio(data->dev);
	dev_set_drvdata(data->dev, data);

	return;
}

static void delay_init_868stick(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	INIT_DELAYED_WORK(&data->ew_work, init_868stick);
	// perform the worker
	schedule_delayed_work(&data->ew_work, msecs_to_jiffies(DELAY_868_MCU));
}

static void ctrl_led(struct device *dev, bool state)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	if (data->led == NULL)
		return;

	if (state)
		// turn LED on, open-drain output thus 0
		gpiod_set_value(data->led, 0);
	else
		gpiod_set_value(data->led, 1);
}

#define LED_OFFSET 2
static void init_cocobox_led(struct work_struct *work)
{
	struct cocobox_data *data = container_of(work, struct cocobox_data, p1_work.work);
	struct gpio_chip *chip;
	struct usb_serial *serial = NULL;
	struct usb_ids match;

	match.vid = COMMON_VID;
	match.pid = P1_PID;

	chip = gpiochip_find(&match, gpiochip_product_match);
	if (chip == NULL) {
		data->p1_work_cnt++;
		if (data->p1_work_cnt < CP210x_RETRY_COUNT) {
			dev_dbg(data->dev, "didn't find led device, retry %d\n", data->p1_work_cnt);
			schedule_delayed_work(&data->p1_work, msecs_to_jiffies(CP210x_DELAY_DRIVER));
		} else {
			dev_err(data->dev, "didn't find led device, given up\n");
		}
		return;
	}

	serial = gpiochip_get_data(chip);
	dev_info(data->dev, "led probed (%d): '%s' '%s' (gpiochip%d)\n", data->p1_work_cnt+1, serial->dev->product,
			serial->dev->manufacturer, chip->base);

	data->led = gpio_to_desc(chip->base + LED_OFFSET);
	if (data->led == NULL) {
		dev_err(data->dev, "couldn't map led gpio number\n");
		return;
	}

	gpiochip_generic_config(chip, LED_OFFSET, PIN_CONFIG_DRIVE_OPEN_DRAIN);
	ctrl_led(data->dev, true);
}

static void delay_init_cocobox_led(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	INIT_DELAYED_WORK(&data->p1_work, init_cocobox_led);
	// perform the worker
	schedule_delayed_work(&data->p1_work, msecs_to_jiffies(CP210x_DELAY_DRIVER));
}

static void deinit_cocobox_led(struct device *dev)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	data->led = NULL;
}

static void probe_zigbee_brick(struct device *dev)
{
	dev_info(dev, "zigbee brick probed\n");
	// nothing more to be done
}

static ssize_t mode_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	sprintf(buf, "%d\n", data->i2c_state);
	return strlen(buf);
}

static ssize_t string_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	if (!strcmp(attr->attr.name, "protocol"))
		sprintf(buf, "%s\n", protocol_description[data->protocol]);
	else if (!strcmp(attr->attr.name, "radiostate"))
		sprintf(buf, "%s\n", state_description[data->mcu_state]);
	else if (!strcmp(attr->attr.name, "swversion"))
		sprintf(buf, "%d.%d", (data->swversion & 0x000000F0)  >> 4, (data->swversion & 0x0000000F));
	else
		return -EINVAL;

	return strlen(buf);
}

static ssize_t radiostate_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct cocobox_data *data = dev_get_drvdata(dev);

	init_radio(data->dev);
	return count;
}

static ssize_t protocol_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	unsigned int regval = 0;
	char input_string[DESC_SIZE];
	int capabilities = 0;

	if (count < 2 || count >= DESC_SIZE)
		return -EINVAL;

	if (data->i2c_state != STATE_INITIALIZED) {
		dev_warn(dev, "cannot store protocol because i2c is not initialized\n");
		return count;
	}

	strcpy(input_string, buf);
	if (count > 0 && input_string[count-1] == '\n')
		input_string[count-1] = 0;

	regmap_read(data->regmap, RADIO_CAPABILITIES, &capabilities);

	if (!strcmp(input_string, protocol_description[P_EASYWAVE])) {
		data->protocol = P_EASYWAVE;
		regval = R2B3_EASYWAVE;
	} else if (!strcmp(input_string, protocol_description[P_EASYWAVESHARP])) {
		data->protocol = P_EASYWAVESHARP;
		regval = R2B5_EASYWAVESHARP;
	} else if (!strcmp(input_string, protocol_description[P_TRANSPARENT])) {
		data->protocol = P_TRANSPARENT;
		regval = R2B0_TRANSPARENT;
	} else {
		return -EINVAL;
	}

	if (~capabilities & regval) {
		dev_err(dev, "Protocol %s not supported by the firmware\n", protocol_description[data->protocol]);
		data->protocol = P_UNSUPPORTED;
		return -EINVAL;
	}

	regmap_write(data->regmap, RADIO_ACTIVE_PROT, regval);
	protocol_settings(data->dev);
	talk_to_udev(data->dev);
	dev_info(dev, "switch to protocol=%s\n", protocol_description[data->protocol]);

	return count;
}

static ssize_t bootloader_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	unsigned int value;

	if (sscanf(buf, "%u", &value) != 1)
		return -EINVAL;

	switch (value) {
		case 1:
			// gentle shutdown
			if (data->i2c_state != STATE_INITIALIZED) {
				dev_err(dev, "i2c radio not operational, try hard reset\n");
				return count;
			}
			dev_info(dev, "go to bootloader, the soft way\n");
			regmap_write(data->regmap, RADIO_BOOT_PWR, R0_BOOTLOADER);
			break;
		case 2:
			dev_info(dev, "go to bootloader, the hard way\n");
			/* hard reset does not allow saving frame counters */
			gpiod_set_value(data->resetpin, 1);
			gpiod_set_value(data->resetpin, 0);
			msleep(1); // measured about 25ms
			gpiod_set_value(data->resetpin, 1);
			break;
		default:
			return count;
	}

	data->mcu_state = STATE_BOOTLOADER;
	data->protocol = P_UNSUPPORTED;

	talk_to_udev(dev);
	schedule_delayed_work(&data->dwork, RADIO_POLL_PERIOD*HZ);

	return count;
}

static ssize_t rssi_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	int val = 0, raw = 0;

	if (data->mcu_state == STATE_APPLICATION)
		regmap_read(data->regmap, RADIO_RSSI, &raw);

	// convert 2's-complement to signed int
	if (raw & 0x80)
		// negative value
		val = raw | ~((1 << 8) - 1);
	else
		val = raw;

	return sprintf(buf, "%+d", val);
}

static ssize_t lqi_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	unsigned int val = 0;

	if (data->mcu_state == STATE_APPLICATION)
		regmap_read(data->regmap, RADIO_LQI, &val);
	val &= 0x7F;
	if (val)
		sprintf(buf, "%d", val);
	else
		sprintf(buf, "-");

	return strlen(buf);
}

static ssize_t led_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned int value;

	if (sscanf(buf, "%u", &value) != 1)
		return -EINVAL;

	if (!value)
		ctrl_led(dev, false);
	else
		ctrl_led(dev, true);

	return count;
}

static ssize_t capabilities_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct cocobox_data *data = dev_get_drvdata(dev);
	unsigned int capabilities = 0;

	regmap_read(data->regmap, RADIO_CAPABILITIES, &capabilities);
	return sprintf(buf, "%02X\n", (capabilities & 0xFF));
}

static DEVICE_ATTR(mode, S_IRUGO, mode_show, NULL);
static DEVICE_ATTR(protocol, S_IWUSR | S_IRUGO, string_show, protocol_store);
static DEVICE_ATTR(radiostate, S_IWUSR | S_IRUGO, string_show, radiostate_store);
static DEVICE_ATTR(swversion, S_IRUGO, string_show, NULL);
static DEVICE_ATTR(bootloader, S_IWUSR, NULL, bootloader_store);
static DEVICE_ATTR(rssi, S_IRUGO, rssi_show, NULL);
static DEVICE_ATTR(lqi, S_IRUGO, lqi_show, NULL);
static DEVICE_ATTR(led, S_IWUSR, NULL, led_mode);
static DEVICE_ATTR(capabilities, S_IRUGO, capabilities_show, NULL);

static struct attribute *attrs[] = {
	&dev_attr_mode.attr,
	&dev_attr_protocol.attr,
	&dev_attr_radiostate.attr,
	&dev_attr_swversion.attr,
	&dev_attr_bootloader.attr,
	&dev_attr_rssi.attr,
	&dev_attr_lqi.attr,
	&dev_attr_led.attr,
	&dev_attr_capabilities.attr,
	NULL
};

static struct attribute_group sysfs_files = { .attrs = attrs, };

static int usbdev_coldplug(struct usb_device *udev, void *p)
{
	struct device *dev = p;

	if (usb_descriptor_check(udev->descriptor, COMMON_VID, EW_STICK_PID))
		delay_init_868stick(dev);

	if (usb_descriptor_check(udev->descriptor, COMMON_VID, P1_PID))
		delay_init_cocobox_led(dev);

	if (usb_descriptor_check(udev->descriptor, COMMON_VID, ZB_BRICK_PID))
		probe_zigbee_brick(dev);

	return 0;
}

static int usbdev_notify(struct notifier_block *nb, unsigned long action, void *data)
{
	struct usbport_trig_data *usbport_data = container_of(nb, struct usbport_trig_data, nb);
	struct device *dev = usbport_data->dev;
	struct usb_device *usb_dev = data;

#if 0
	u16 vid = le16_to_cpu(usb_dev->descriptor.idVendor);
	u16 pid = le16_to_cpu(usb_dev->descriptor.idProduct);
	dev_info(dev, "USB DEVICE: action:%d VID:%04X PID:%04X\n", (int)action, vid, pid);
#endif

	if (usb_descriptor_check(usb_dev->descriptor, COMMON_VID, EW_STICK_PID)) {
		switch (action) {
		case USB_DEVICE_ADD:
			delay_init_868stick(dev);
			break;
		case USB_DEVICE_REMOVE:
			deinit_868stick(dev);
			clear_data(dev);
			break;
		}
	}

	if (usb_descriptor_check(usb_dev->descriptor, COMMON_VID, P1_PID)) {
		switch (action) {
		case USB_DEVICE_ADD:
			delay_init_cocobox_led(dev);
			break;
		case USB_DEVICE_REMOVE:
			deinit_cocobox_led(dev);
			break;
		}
	}

	if (usb_descriptor_check(usb_dev->descriptor, COMMON_VID, ZB_BRICK_PID)) {
		switch (action) {
		case USB_DEVICE_ADD:
			probe_zigbee_brick(dev);
			break;
		case USB_DEVICE_REMOVE:
			break;
		}
	}

	return NOTIFY_OK;
}

static int cocobox_probe(struct platform_device *pdev)
{
	int ret = 0;
	struct cocobox_data *data;

	data = kzalloc(sizeof(*data), GFP_KERNEL);
	if (!data) {
		dev_err(&pdev->dev, "malloc failed\n");
		return -ENOMEM;
	}
	data->dev = &pdev->dev;
	dev_set_drvdata(&pdev->dev, data);
	clear_data(&pdev->dev);
	data->led = NULL;

	ret = sysfs_create_group(&pdev->dev.kobj, &sysfs_files);
	if (ret) {
		dev_err(&pdev->dev, "failed to create sysfs\n");
		kfree(data);
		return ret;
	}

	//  creating platform independent link under /sys/kernel/emperor/
	ret = sysfs_create_link(emperor_kobj, &pdev->dev.kobj, DRIVER_NAME);
	if (ret) {
		dev_err(&pdev->dev, "failed to link sysfs %s\n", DRIVER_NAME);
		kfree(data);
		return ret;
	}
	ret = sysfs_create_link(emperor_kobj, &pdev->dev.kobj, RADIONAME);
	if (ret) {
		dev_err(&pdev->dev, "failed to link sysfs %s\n", RADIONAME);
		kfree(data);
		return ret;
	}

	data->reg_usbh1 = devm_regulator_get_optional(data->dev, "usb_h1_vbus");
	if (IS_ERR(data->reg_usbh1)) {
		dev_err(&pdev->dev, "failed to get regulator usbh1\n");
	} else {
		if (regulator_enable(data->reg_usbh1))
			dev_err(&pdev->dev, "failed to enable regulator usbh1\n");
	}

	data->reg_usbotg = devm_regulator_get_optional(data->dev, "usb_otg_vbus");
	if (IS_ERR(data->reg_usbotg)) {
		dev_err(&pdev->dev, "failed to get regulator usbotg\n");
	} else {
		if (regulator_enable(data->reg_usbotg))
			dev_err(&pdev->dev, "failed to enable regulator usbotg\n");
	}

	// USB cold plugged devices
	usb_for_each_dev(&pdev->dev, usbdev_coldplug);
	// USB hot plugged devices
	usbport_data = kzalloc(sizeof(*usbport_data), GFP_KERNEL);
	if (usbport_data) {
		usbport_data->dev = &pdev->dev;
		usbport_data->nb.notifier_call = usbdev_notify;
		usb_register_notify(&usbport_data->nb);
	} else {
		dev_info(&pdev->dev, "hotplugging failed\n");
	}

	INIT_DELAYED_WORK(&data->dwork, poll_radio);
	dev_info(&pdev->dev, "driver successfully loaded\n");
	return ret;
}

static int cocobox_remove(struct platform_device *pdev)
{
	struct cocobox_data *data = dev_get_drvdata(&pdev->dev);

	usb_unregister_notify(&usbport_data->nb);
	deinit_868stick(&pdev->dev);
	deinit_cocobox_led(&pdev->dev);
	sysfs_remove_link(emperor_kobj, RADIONAME);
	sysfs_remove_link(emperor_kobj, DRIVER_NAME);
	sysfs_remove_group(&pdev->dev.kobj, &sysfs_files);
	platform_set_drvdata(pdev, NULL);
	kfree(data);
	kfree(usbport_data);
	dev_info(&pdev->dev, "driver successfully removed\n");

	return 0;
}

static void cocobox_shutdown(struct platform_device *pdev)
{
	struct cocobox_data *data = dev_get_drvdata(&pdev->dev);

	dev_info(&pdev->dev, "power off USB ports\n");

	if (regulator_force_disable(data->reg_usbh1))
		dev_err(&pdev->dev, "failed to disable regulator usbh1\n");
	if (regulator_force_disable(data->reg_usbotg))
		dev_err(&pdev->dev, "failed to disable regulator usbotg\n");
}

static const struct of_device_id cocobox_id_table[] = {
	{ .compatible = DRIVER_NAME, },
	{ }
};

MODULE_DEVICE_TABLE(of, cocobox_id_table);

static struct platform_driver emperor_cocobox_driver = {
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = cocobox_id_table,
	},
	.probe = cocobox_probe,
	.remove = cocobox_remove,
	.shutdown = cocobox_shutdown,
};

module_platform_driver(emperor_cocobox_driver);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Wim De Witte <de.witte.wim@gmail.com>");
