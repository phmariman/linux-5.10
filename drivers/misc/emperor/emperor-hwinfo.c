/*
 * This driver exposes some specific emperor hardware information
 * The hardware type is passed by a kernel argument by u-boot.
 *   Details about the hardware can be found in /sys/kernel/emperor:
 *     empid: hardware revision of the emperor
 *     hubid: base board type
 *     lanbrick: detection of a lanbrick
 *     bootcountvalue: uses for interaction with u-boot (stored in a volatile register)
 */

#include <linux/init.h>
#include <linux/kobject.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/platform_device.h>
#include <linux/pm_runtime.h>
#include <linux/clk.h>
#include <asm/io.h>
#include <linux/if_ether.h>
#include <linux/io.h>
#include <linux/nvmem-consumer.h>
#include <linux/regulator/consumer.h>
#include <linux/nvmem-consumer.h>
#include <linux/rtc.h>

#define DRIVER_NAME	"emperor-hwinfo"

/* Convert GPIO signal to GPIO pin number */
#define GPIO_TO_PIN(bank, gpio) (32 * (bank) + (gpio))

/*
 * These enums are an exact copy from u-boot,
 * have to match because these types are passed through the kernel command line.
 */

/* Emperor type */
typedef enum {
	EMP_COCO_V0 = 4,	// Proto0
	EMP_COCO_V1,	// Proto1/series
	EMP_SMARTBOX,	// 256MB RAM, 256MB NAND, 528MHz
	EMP_SMARTBOX_PLUS, // 512MB RAM, 1GB NAND, 900MHz
	EMP_UNKNOWN,
} EMPEROR_TYPE;

/* Fifthub types */
typedef enum {
	HUB_NONE = 0,
	HUB_COCO = 5,		// Compact Controller
	HUB_SMARTBOX = 7,	// GW3.0
	HUB_UNKNOWN,
} HUB_TYPE;

const char *hubtype_list[]= {
	"none",
	"none",
	"none",
	"none",
	"none",
	"COCO",
	"none",
	"SMARTBOX",
	"unknown",
};

typedef enum {
	LANPHY_ERROR = 0,
	LANPHY_NOBRICK,
	LANPHY_LANBRICK,
	LANPHY_SWITCH,
} LANPHY_TYPE;

typedef enum {
	CPU_UNKNOWN = 0,
	CPU_IMX6DL,
	CPU_IMX6UL,
} CPU_FAMILY;

#define FPSERIAL_NAME	"fpserial"
#define FPSERIAL_LENGTH	10
#define MAC_NAME		"macwan"
#define MAC_LENGTH		12
#define PWSALT_NAME		"pwsalt"
#define PWSALT_LENGTH	8
#define PWHASH_NAME		"pwhash"
#define PWHASH_LENGTH	22
#define HTTPS_NAME		"https"
#define HTTPS_LENGTH	16
#define WPA_NAME		"wpakey"
#define WPA_LENGTH		32
#define MFGRSN_NAME		"mfgrserial"
#define MFGRSN_LENGTH	12
#define SKU_NAME		"sku"
#define SKU_LENGTH		1

#define NIKO_OUI		"00112A"
#define OVERRIDE_PW		"root:$1$O1.uatjL$IAhRgDlevaHXZ5J9wkOBs1:::::::"

// global variables
struct hwinfo_data {
	int registered;
	int override;
	LANPHY_TYPE lanbrick;
	HUB_TYPE hubversion;
	EMPEROR_TYPE emperorversion;
	CPU_FAMILY cpufamily;
	const char *hubstring;
	const char *bootmode;
	const char *cpuid;
	char fpserial[FPSERIAL_LENGTH+1];
	char macwan[MAC_LENGTH+1];
	char pwsalt[PWSALT_LENGTH+1];
	char pwhash[PWHASH_LENGTH+1];
	char https[HTTPS_LENGTH+1];
	char wpakey[WPA_LENGTH+1];
	char mfgrserial[MFGRSN_LENGTH+1];
	char sku[SKU_LENGTH+1];
};

struct hwinfo_data *data;

struct class *emperor_class;
EXPORT_SYMBOL_GPL(emperor_class);

struct device *gpio_dev;
EXPORT_SYMBOL_GPL(gpio_dev);

struct kobject *emperor_kobj;
EXPORT_SYMBOL_GPL(emperor_kobj);

HUB_TYPE hubtype_str2int(const char *hubname)
{
	int i;

	for (i=1; i < HUB_UNKNOWN; i++)
		if (strcmp(hubname, hubtype_list[i]) == 0)
			// command matches
			return i;
	return HUB_UNKNOWN;
}

int query_emperorversion(void)
{
	return (int)data->emperorversion;
}
EXPORT_SYMBOL_GPL(query_emperorversion);

int setup_gpio_pin(struct device_node *dn, const char *label, unsigned long flags)
{
	int status = 0;
	int gpio = 0;
	enum of_gpio_flags of_flags;
	
	gpio = of_get_named_gpio_flags(dn, label, 0, &of_flags); 
    if (gpio < 0) {
		pr_err("%s: Failed to read %s\n", DRIVER_NAME, label);
		return -1;
	}
	if (of_flags & OF_GPIO_ACTIVE_LOW)
		flags |= GPIOF_INIT_HIGH;
	
	status = gpio_request_one(gpio, flags, label);
	if (status < 0) {
		pr_err("%s: Failed to request gpio %d for %s\n", DRIVER_NAME, gpio, label);
		return -1;
	}
	
	if (flags & GPIOF_EXPORT) {
		gpio_export(gpio, true);
		gpio_export_link(gpio_dev, label, gpio);
	}
	
	return gpio;
}
EXPORT_SYMBOL_GPL(setup_gpio_pin);

static ssize_t hubid_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", data->hubversion);
}

static ssize_t hubtype_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%s\n", data->hubstring);
}

static ssize_t bootmode_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%s\n", data->bootmode);
}

static ssize_t empid_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", data->emperorversion);
}

static ssize_t lanbrick_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", data->lanbrick);
}

#define IXM6_SRC_GPR9 0x40

static ssize_t bootcount_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	struct device_node *np;
	void __iomem *src_base;
	u32 value = 0;

	if (data->cpufamily == CPU_IMX6DL) {
		np = of_find_compatible_node(NULL, NULL, "fsl,imx6q-src");
	} else if (data->cpufamily == CPU_IMX6UL) {
		np = of_find_compatible_node(NULL, NULL, "fsl,imx6ul-src");
	} else
		return sprintf(buf, "%d\n", value);

	src_base = of_iomap(np, 0);
	if (!src_base) {
		pr_info("%s: bootcount_store failed to get memory\n", DRIVER_NAME);
	} else {
		value = readl_relaxed(src_base + IXM6_SRC_GPR9);
		iounmap(src_base);
	}

	return sprintf(buf, "%d\n", value);
}

static ssize_t bootcount_store(struct kobject * kobj, struct kobj_attribute * attr, const char * buf, size_t count)
{
	unsigned int value;
	struct device_node *np;
	void __iomem *src_base;

	if (sscanf(buf, "%u", &value) != 1)
		return -EINVAL;

	if (data->cpufamily == CPU_IMX6DL) {
		np = of_find_compatible_node(NULL, NULL, "fsl,imx6q-src");
	} else if (data->cpufamily == CPU_IMX6UL) {
		np = of_find_compatible_node(NULL, NULL, "fsl,imx6ul-src");
	} else
		return count;

	src_base = of_iomap(np, 0);
	if (!src_base) {
		pr_info("%s: bootcount_store failed to get memory\n", DRIVER_NAME);
	} else {
		writel_relaxed((u32)value, src_base + IXM6_SRC_GPR9);
		iounmap(src_base);
	}
	return count;
}

static ssize_t fifthgateconf_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "httppass=%s\n", data->https);
}

static ssize_t wpakey_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%s\n", data->wpakey);
}

static ssize_t fpserial_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%s\n", data->fpserial);
}

static ssize_t override_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", data->override);
}

static ssize_t override_store(struct kobject * kobj, struct kobj_attribute * attr, const char * buf, size_t count)
{
	unsigned int val;

	if (sscanf(buf, "%u", &val) != 1)
		return -EINVAL;

	if (val)
		pr_info("%s: root password override\n", DRIVER_NAME);

	data->override = val;

	return count;
}

static ssize_t shadow_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	if (data->override || !data->registered)
		return sprintf(buf, "%s\n", OVERRIDE_PW);
	else
		return sprintf(buf, "root:$1$%s$%s:::::::\n", data->pwsalt, data->pwhash);
}

static ssize_t hostname_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "FP%s\n", data->registered ? data->macwan : data->cpuid);
}

static ssize_t nhcmac_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	unsigned long long macvalue = 0, oui = 0;
	int ret, nhcmac = 0;

	if (!data->registered)
		return sprintf(buf, "00000000\n");

	ret = kstrtoull(data->macwan, 16, &macvalue);
	ret = kstrtoull(NIKO_OUI "000000", 16, &oui);

	// substract the OUI
	nhcmac = (int)(macvalue - oui);
	// limit to 18 bits
	nhcmac &= 0x3FFFF;
	// add the coco ID type
	nhcmac += (0x3B << 18);

	return sprintf(buf, "%08X\n", nhcmac);
}

static struct kobj_attribute bootcount_attribute =
	__ATTR(bootcountvalue, 0644, bootcount_show, bootcount_store);

static struct kobj_attribute hubid_attribute =
	__ATTR(hubid, 0444, hubid_show, NULL);

static struct kobj_attribute hubtype_attribute =
	__ATTR(hubtype, 0444, hubtype_show, NULL);

static struct kobj_attribute bootmode_attribute =
	__ATTR(bootmode, 0444, bootmode_show, NULL);

static struct kobj_attribute empid_attribute =
	__ATTR(empid, 0444, empid_show, NULL);

static struct kobj_attribute lanbrick_attribute =
	__ATTR(lanbrick, 0444, lanbrick_show, NULL);

static struct kobj_attribute fifthgateconf_attribute =
	__ATTR(fifthgate.conf, 0444, fifthgateconf_show, NULL);

static struct kobj_attribute wpakey_attribute =
	__ATTR(wpakey, 0444, wpakey_show, NULL);

static struct kobj_attribute fpserial_attribute =
	__ATTR(fpserial, 0444, fpserial_show, NULL);

static struct kobj_attribute override_attribute =
	__ATTR(override, 0644, override_show, override_store);

static struct kobj_attribute shadow_attribute =
	__ATTR(shadow, 0444, shadow_show, NULL);

static struct kobj_attribute hostname_attribute =
	__ATTR(HOSTNAME, 0444, hostname_show, NULL);

static struct kobj_attribute nhcmac_attribute =
	__ATTR(nhcmac, 0444, nhcmac_show, NULL);

static struct attribute *attrs[] = {
	&hubid_attribute.attr,
	&hubtype_attribute.attr,
	&bootmode_attribute.attr,
	&empid_attribute.attr,
	&lanbrick_attribute.attr,
	&bootcount_attribute.attr,
	&fifthgateconf_attribute.attr,
	&wpakey_attribute.attr,
	&fpserial_attribute.attr,
	&override_attribute.attr,
	&shadow_attribute.attr,
	&hostname_attribute.attr,
	&nhcmac_attribute.attr,
	NULL,
};

static struct attribute_group attr_group = {
	.attrs = attrs,
};

static void create_coco_gpios(struct device_node *dn)
{
	setup_gpio_pin(dn, "rst_mcu", (GPIOF_DIR_OUT | GPIOF_EXPORT));
}

static void create_smartbox_gpios(struct device_node *dn)
{
	int gpio = 0;
	struct gpio_desc *gpiod;

	setup_gpio_pin(dn, "rst_zwave", (GPIOF_DIR_OUT | GPIOF_EXPORT));
	setup_gpio_pin(dn, "rst_brick", (GPIOF_DIR_OUT | GPIOF_EXPORT));
	setup_gpio_pin(dn, "sku_zwave", (GPIOF_DIR_IN | GPIOF_EXPORT));
	setup_gpio_pin(dn, "sku_redpine", (GPIOF_DIR_IN | GPIOF_EXPORT));

	gpio = setup_gpio_pin(dn, "rst_redpine", (GPIOF_DIR_OUT | GPIOF_EXPORT));
	gpiod = gpio_to_desc(gpio);
	gpiod_set_value(gpiod, 1);
	gpiod_set_value(gpiod, 0);
	msleep(20);
	gpiod_set_value(gpiod, 1);
}

// optional setting
static void smartbox_vdd_usb_cap(struct device dev)
{
	int retval = 0;
	struct regulator *reg_usb;
	int voltage = 3000000; // uV

	reg_usb = devm_regulator_get_optional(&dev, "phy-3p0");
	if (IS_ERR(reg_usb))
		return;

	retval = regulator_enable(reg_usb);
	if (retval) {
		dev_err(&dev, "Failed to enable phy-3p0 regulator: %d\n", retval);
		return;
	}

	retval = regulator_set_voltage(reg_usb, voltage, voltage);
	if (retval) {
		dev_err(&dev, "Failed to set phy-3p0 regulator: %d\n", retval);
	}
}

static const struct of_device_id emperor_hwinfo_id_table[] = {
	{
		.compatible	= DRIVER_NAME,
	},
	{}
};
MODULE_DEVICE_TABLE(of, emperor_hwinfo_id_table);

static int of_hardware_info(struct platform_device *pdev)
{
	int retval = 0;
	long int convert = 0;
	const char *of_string;
	struct device_node *dn = pdev->dev.of_node;

	retval = of_property_read_string(dn, "lanbrick", &of_string);
	if (retval < 0) {
		dev_err(&pdev->dev, "Failed to read lanbrick\n");
		data->lanbrick = LANPHY_ERROR;
	} else {
		retval = kstrtol(of_string, 10, &convert);
		if (retval < 0)
			data->lanbrick = LANPHY_ERROR;
		
		data->lanbrick = (LANPHY_TYPE)convert;
	}

	retval = of_property_read_string(dn, "bootmode", &data->bootmode);
	if (retval < 0)
		dev_info(&pdev->dev, "Failed to read bootmode\n");

	retval = of_property_read_string(dn, "emperor_rev", &of_string);
	if (retval < 0) {
		dev_err(&pdev->dev, "Failed to read emperor_rev\n");
		return retval;
	}
	retval = kstrtol(of_string, 10, &convert);
	if (retval < 0) {
		dev_err(&pdev->dev, "Failed to convert emperor_rev\n");
		return retval;
	}
	data->emperorversion = (EMPEROR_TYPE)convert;

	if (data->emperorversion == EMP_COCO_V0 || data->emperorversion == EMP_COCO_V1)
		data->cpufamily = CPU_IMX6DL;
	else if (data->emperorversion == EMP_SMARTBOX || data->emperorversion == EMP_SMARTBOX_PLUS)
		data->cpufamily = CPU_IMX6UL;

	retval = of_property_read_string(dn, "hubtype", &data->hubstring);
	if (retval < 0) {
		dev_err(&pdev->dev, "Failed to read hubtype\n");
		return retval;
	}

	retval = of_property_read_string(dn, "cpuid", &data->cpuid);
	if (retval < 0) {
		dev_err(&pdev->dev, "Failed to read cpuid\n");
		return retval;
	}
	
	data->hubversion = hubtype_str2int(data->hubstring);
	
	dev_info(&pdev->dev, "Emperor (%d), Hub (%d=%s), Bootmode (%s), LAN (%d), %s\n", \
		data->emperorversion, data->hubversion, data->hubstring, data->bootmode, data->lanbrick, \
		data->registered ? "registered" : "unregistered");

	if (strncmp("COCO", data->hubstring, 4) == 0)
		create_coco_gpios(dn);

	if (strncmp("SMARTBOX", data->hubstring, 4) == 0) {
		smartbox_vdd_usb_cap(pdev->dev);
		create_smartbox_gpios(dn);
	}

	return retval;
}

static void initialize_hwinfo_data(struct device *dev)
{
	data->registered = 0;
	data->override = 0;
	data->lanbrick = LANPHY_NOBRICK;
	data->hubversion = HUB_UNKNOWN;
	data->emperorversion = EMP_UNKNOWN;
	data->cpufamily = CPU_UNKNOWN;
	data->hubstring = "unknown";
	data->bootmode = "unknown";
}

static int get_eeprom_cell(struct device *dev, const char *cell_id, size_t val_len)
{
	char *value;
	size_t cell_len;
	struct nvmem_cell *cell;

	cell = nvmem_cell_get(dev, cell_id);
	if (IS_ERR(cell)) {
		if (PTR_ERR(cell) == -EPROBE_DEFER)
			return PTR_ERR(cell);
		return -EINVAL;
	}

	value = nvmem_cell_read(cell, &cell_len);
	nvmem_cell_put(cell);
	if (IS_ERR(value)) {
		return -EINVAL;
	}

	if (cell_len != val_len) {
		kfree(value);
		return -EINVAL;
	}

	val_len++;

	if (!strcmp(cell_id, FPSERIAL_NAME))
		snprintf(data->fpserial, val_len, "%s", value);
	else if (!strcmp(cell_id, MAC_NAME))
		snprintf(data->macwan, val_len, "%s", value);
	else if (!strcmp(cell_id, PWSALT_NAME))
		snprintf(data->pwsalt, val_len, "%s", value);
	else if (!strcmp(cell_id, PWHASH_NAME))
		snprintf(data->pwhash, val_len, "%s", value);
	else if (!strcmp(cell_id, HTTPS_NAME))
		snprintf(data->https, val_len, "%s", value);
	else if (!strcmp(cell_id, WPA_NAME))
		snprintf(data->wpakey, val_len, "%s", value);
	else if (!strcmp(cell_id, MFGRSN_NAME))
		snprintf(data->mfgrserial, val_len, "%s", value);
	else if (!strcmp(cell_id, SKU_NAME))
		snprintf(data->sku, val_len, "%s", value);

	kfree(value);
	return 0;
}

static void get_eeprom_data(struct device *dev)
{
	int i, mfgrvalue = 0;

	if (get_eeprom_cell(dev, FPSERIAL_NAME, FPSERIAL_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", FPSERIAL_NAME);

	if (get_eeprom_cell(dev, MAC_NAME, MAC_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", MAC_NAME);

	if (get_eeprom_cell(dev, PWSALT_NAME, PWSALT_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", PWSALT_NAME);

	if (get_eeprom_cell(dev, PWHASH_NAME, PWHASH_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", PWHASH_NAME);

	if (get_eeprom_cell(dev, HTTPS_NAME, HTTPS_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", HTTPS_NAME);

	if (get_eeprom_cell(dev, WPA_NAME, WPA_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", WPA_NAME);

	if (get_eeprom_cell(dev, MFGRSN_NAME, MFGRSN_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", MFGRSN_NAME);

	if (get_eeprom_cell(dev, SKU_NAME, SKU_LENGTH))
		dev_err(dev, "Failed to read eeprom data %s\n", SKU_NAME);

	for (i=0; i < MFGRSN_LENGTH; i++)
		mfgrvalue += data->mfgrserial[i];

	if (mfgrvalue != (12*0xFF))
		data->registered = 1;
}

static int emperor_hwinfo_probe(struct platform_device *pdev)
{
	int retval = 0;

	data = kzalloc(sizeof(*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	initialize_hwinfo_data(&pdev->dev);

	// object exist already, the first probe resulted in a deferred probe
	if (emperor_kobj)
		goto probe_nvmem;

	//  creating virtual files under /sys/kernel/emperor
	emperor_kobj = kobject_create_and_add("emperor", kernel_kobj);
	if (!emperor_kobj) {
		dev_err(&pdev->dev, "failed to create emperor sysfs\n");
		return -ENOMEM;
	}

	retval = sysfs_create_group(emperor_kobj, &attr_group);
	if (retval)
		kobject_put(emperor_kobj);

	emperor_class = class_create(THIS_MODULE, "emperor");
	if (IS_ERR(emperor_class)) {
		dev_err(&pdev->dev, "Failed to create class emperor\n");
		return -EINVAL;
	}

	//  creating virtual files under /sys/class/emperor/gpio
	gpio_dev = device_create(emperor_class, NULL, 0, NULL, "gpio");
	if (IS_ERR(gpio_dev)) {
		dev_err(&pdev->dev, "Failed to create device gpio\n");
		return -EINVAL;
	}

	retval = get_eeprom_cell(&pdev->dev, MAC_NAME, MAC_LENGTH);
	if (retval == -EPROBE_DEFER)
		return retval;

probe_nvmem:
	get_eeprom_data(&pdev->dev);

	// get hardware specific info from the device tree
	if (of_hardware_info(pdev))
		dev_err(&pdev->dev, "Failed to read data from devicetree\n");

	return retval;
}

static int emperor_hwinfo_remove(struct platform_device *pdev)
{
	kfree(data);
	device_unregister(gpio_dev);
	class_destroy(emperor_class);
	sysfs_remove_group(emperor_kobj, &attr_group);
	kobject_put(emperor_kobj);

	pr_info("%s: driver successfully removed\n", DRIVER_NAME);

	return 0;
}

static struct platform_driver emperor_hwinfo_driver = {
	.driver		= {
		.name	= DRIVER_NAME,
		.owner	= THIS_MODULE,
		.of_match_table = emperor_hwinfo_id_table,
	},
	.probe		= emperor_hwinfo_probe,
	.remove		= emperor_hwinfo_remove,
};

module_platform_driver(emperor_hwinfo_driver);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Wim De Witte <de.witte.wim@gmail.com>");
