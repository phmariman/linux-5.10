/*
 * emperor-radio.h
 *
 *  Created on: Dec 22, 2020
 *      Author: wdw
 */

#ifndef DRIVERS_MISC_EMPEROR_EMPEROR_RADIO_H_
#define DRIVERS_MISC_EMPEROR_EMPEROR_RADIO_H_


/* Radio Registers */
#define RADIO_BOOT_PWR		0x00
#define RADIO_FWVERSION		0x01
#define RADIO_CAPABILITIES	0x02
#define RADIO_ACTIVE_PROT	0x03
#define RADIO_TEMPERATURE	0x04
#define RADIO_SLPMAC0		0x05
#define RADIO_SLPMAC1		0x06
#define RADIO_SLPMAC2		0x07
#define RADIO_SLPMAC3		0x08
#define RADIO_WMBUSMAC0		0x09
#define RADIO_WMBUSMAC1		0x0a
#define RADIO_WMBUSMAC2		0x0b
#define RADIO_WMBUSMAC3		0x0c
#define RADIO_UART			0x0d
#define RADIO_SCRATCH		0x0e
#define RADIO_GPIO_GET		0x0f
#define RADIO_GPIO_INIT		0x10
#define RADIO_GPIO_SET		0x11
#define RADIO_UART_GET		0x12
#define RADIO_UART_SET		0x13
#define RADIO_RF_TEST		0x14
#define RADIO_SPI_TEST		0x15
#define RADIO_SLP_MASTER0	0x16
#define RADIO_SLP_MASTER1	0x17
#define RADIO_SLP_MASTER2	0x18
#define RADIO_SLP_MASTER3	0x19
#define RADIO_SLP_MASTER4	0x1a
#define RADIO_SLP_MASTER5	0x1b
#define RADIO_SLP_MASTER6	0x1c
#define RADIO_SLP_MASTER7	0x1d
#define RADIO_SLP_MASTER8	0x1e
#define RADIO_SLP_MASTER9	0x1f
#define RADIO_SLP_MASTERA	0x20
#define RADIO_SLP_MASTERB	0x21
#define RADIO_SLP_MASTERC	0x22
#define RADIO_SLP_MASTERD	0x23
#define RADIO_SLP_MASTERE	0x24
#define RADIO_SLP_MASTERF	0x25
#define RADIO_RSSI			0x26
#define RADIO_LQI			0x27
#define RADIO_EW_FREQ_OFF0	0x28 // Easywave center frequency offset
#define RADIO_EW_FREQ_OFF1	0x29 // Easywave center frequency offset
#define RADIO_EWS_MAC0		0x2a // Easywavesharp mac address
#define RADIO_EWS_MAC1		0x2b // Easywavesharp mac address
#define RADIO_EWS_MAC2		0x2c // Easywavesharp mac address
#define RADIO_REGISTERS		RADIO_EWS_MAC2

/* Radio Register Bits */
#define R0_NORMALMODE		0x00
#define R0_BOOTLOADER		0x01
#define R0_LOWPOWER			0x02
#define R0_SELFTEST			0x04
#define R2B0_TRANSPARENT	0x01
#define R2B1_SLP			0x02
#define R2B2_WMBUS			0x04
#define R2B3_EASYWAVE		0x08
//#define R2B4_SLP_v2		0x10
#define R2B5_EASYWAVESHARP	0x20
#define R13_UART115K2		0x00
#define R13_UART230K4		0x01
#define R13_UART57K6		0x02
#define R13_UART2400E1		0x03
#define R13_UART9600		0x04
#define R14_SCRATCHVALUE	0x5a

#define DESC_SIZE			20
#define MAC_SIZE			12
#define SLPMAC_SIZE			8
#define SLPMASTERKEY_SIZE	32
#define EWSMAC_SIZE			6

#define SLPBANK				"20"
#define EWSBANK				"00"


/* Radio I2C addresses */
#define RADIO1_ADDR			0x45
#define RADIO2_ADDR			0x44


typedef enum {
	RADIO1 = 1,
	RADIO2
} RADIONR;

typedef enum {
	P_UNSUPPORTED,
	P_SLP,
	P_WMBUS,
	P_EASYWAVE,
	P_EASYWAVESHARP,
	P_TRANSPARENT
} PROTOCOLS;

typedef enum {
	S_ERROR = 0,
	S_BOOTSYSTEM,
	S_NORADIO,
	S_BOOTLOADER,
	S_RADIOFF,
	S_SELFTEST,
	S_OPERATIONAL,
} RADIOSTATE;

const char *protocol_description[] = {
	[P_UNSUPPORTED] =	"unsupported",
	[P_SLP] =			"slp",
	[P_WMBUS] =			"wmbus",
	[P_EASYWAVE] =		"easywave",
	[P_EASYWAVESHARP]	="easywavesharp",
	[P_TRANSPARENT] =	"transparent"
};

const char *state_description[] = {
	[S_ERROR] =			"error",
	[S_BOOTSYSTEM] =	"booting",
	[S_NORADIO] =		"noradio",
	[S_RADIOFF] =		"radiooff",
	[S_SELFTEST] =		"selftest",
	[S_BOOTLOADER] =	"bootloader",
	[S_OPERATIONAL] =	"operational"
};

static const struct regmap_config radio_regmap_config = {
	.reg_bits = 8,
	.val_bits = 8,
	.max_register = RADIO_REGISTERS,
	.cache_type = REGCACHE_NONE,
};

#endif /* DRIVERS_MISC_EMPEROR_EMPEROR_RADIO_H_ */
