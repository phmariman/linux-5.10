/*
 * Radio driver for Smartbox
 *
 * Copyright (C) 2016 Wim De Witte <de.witte.wim (at) gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
 */

/*
 * I²C driver to interface ISM radio's on the fifthplay smartbox.
 *
 *  Radio 1 is always present and has a performant PCB track antenna.
 *  Radio 2 is optional, presence is indicated by means of a gpio strap (sku gpio).
 *  	If a strap gpio is not possible, a label can indicate
 *
 *  Each radio can be controlled with a reset gpio line.
 *  If the Kinetis MCU gets a reset, it jumps for 2 seconds in a bootloader,
 *  	where it can download a firmware.
 *  The MCU pulls a gpio pin high, if it is application mode.
 *
 *  Radio is set to a specific protocol (slp, wmbus, easywave, easywavesharp, transparent).
 *  Radios are not aware of the settings of the other.
 *  A userspace daemon shall select sensitive settings (e.g. avoid same protocols)
 *
 *  Device-tree setup; as a child of an I²C master node (i2c2 on smartbox)
 *	---------------
 *  	smartradio2@45 {
 *			compatible = "smartradio";
 *			reg = <0x45>;
 *			pagesize = <1>;
 *			pinctrl-names = "default";
 *			pinctrl-0 = <&smartradio2_gpio>;
 *			sku-gpio = <&gpio5 3 GPIO_ACTIVE_LOW>;		-> optional to indicate radio presence
 *			reset-gpio = <&gpio1 22 GPIO_ACTIVE_LOW>;	-> optional, bootloader jump also via SW
 *			state-gpio = <&gpio5 1 GPIO_ACTIVE_HIGH>;	-> optional, but more accurate
 *			nvmem-cells = <&eeprom_macwan>;				-> eeprom nvmem cell
 *			nvmem-cell-names = "macwan";				-> eeprom nvmem cell
 *			macwan = "00112A21F327";					-> optional, as alternative for nvmem
 *			tty = "ttymxc6";							-> name of the connected uart, used for udev
 *		};
 *
 *	sysfs interface
 *	---------------
 *	For each radio, a link (radioX) is created under /sys/kernel/emperor
 *	Following files are created (besides the normal i2c slave files):
 *		bootloader (wo)		echo 1 for a soft reboot (saving critical data); 2 for a hard reboot
 *							4 to hold MCU in reset, 3 to release the reset line
 *		capabilities (ro)	shows which RF protocols the firmware has onboard
 *		protocol (rw)		show/store the active protocol
 *		radiostate (ro)		state of the radio
 *		selftest (rw)		a 1 puts the firmware in selftest mode
 *		swversion (ro)		shows the firmware version
 *		reg_addr (rw)		direct register access MCU, address byte
 *		reg_val (rw)		direct register access MCU, data byte
 *		slpmasterkey (wo)	program slp master key during manufacturing, stored non-volatile in MCU
 *		rssi (ro)			rssi value
 *		lqi (ro)			link quality indicator
 *
 *	udev interface
 *	---------------
 *	If the protocol has changed, a message is sent to udev.
 *	Following udev environment variables are set:
 *		RADIO		number of the radio
 *		PROTOCOL	active protocol
 *		STATE		state of the radio
 *		TTY			connected uart
 *	Match rules on DRIVER=="smartradio"
 *	udev script sets the device link to the connected uart depending the protocol
 *		/dev/slp /dev/wmbus /dev/easywave /dev/easywavesharp /dev/transparentX /dev/bootloaderX
 *
 *	hwmon interface
 *	---------------
 *	Integrate hwmon temperature sensors
 *
 *	eeprom interface
 *	---------------
 *	MAC address is read from eeprom, transformed to slp address and programmed in MCU
 *	If no eeprom interface is available, a devicetree label can be used for the MAC
 *
 */

#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/regmap.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/gpio/consumer.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/sysfs.h>
#include <linux/timer.h>
#include <linux/string.h>
#include <linux/nvmem-consumer.h>
#include <linux/workqueue.h>
#include <linux/jiffies.h>
#include <linux/ctype.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>

#include "emperor-radio.h"

#define DRIVER_NAME	"smartradio"

#define MIN_POLL_PERIOD		3	// in seconds: bootloader time + some extra
#define MAX_POLL_CYCLES		7	// time between polls is quadratical

extern struct kobject *emperor_kobj;

/* device data structure */
struct smartradio_data {
	RADIONR radio;					// Radio number
	struct device *dev;				// Pointer to the device structure
	struct i2c_client *client;		// Pointer to the I2C client
	struct regmap *regmap;			// Devices register map
	unsigned int poll_cycles;		// Number of polls
	char sysfslink[12];				// device link under /sys/kernel/emperor
	struct gpio_desc *gpio_reset;	// GPIO to put MCU in bootloader mode
	struct gpio_desc *gpio_state;	// GPIO indicating operational mode
	PROTOCOLS protocol;				// Selected protocol
	RADIOSTATE state;				// State of the radio
	unsigned int assembled;			// Indicates if the radio is assembled
	unsigned int i2c_reg;			// Register address to access
	char macwan[MAC_SIZE+1];		// MAC address gateway
	struct delayed_work dwork;		// Worker for probing the MCU
	struct device *hwmon_dev;		// Pointer to hwmon device
	char tty[DESC_SIZE];			// Name of the connected UART
	int swversion;					// sw version
};

static void talk_to_udev(struct device *dev);
static void hardreset(struct device *dev);

static u8 xdigit_to_i(u8 c)
{
	u8 ret;

	if (isdigit(c))
		ret = c - '0';
	else if ('A' <= c && c <= 'F')
		ret = c - 'A' + 10;
	else if ('a' <= c && c <= 'f')
		ret = c - 'a' + 10;
	else
		ret = 0xF;

	return ret;
}

static int radio_lifetest(struct device *dev)
{
	int ret;
	unsigned int val;
	struct smartradio_data *data = dev_get_drvdata(dev);

	ret = regmap_write(data->regmap, RADIO_SCRATCH, R14_SCRATCHVALUE);
	if (ret)
		return 0;

	ret = regmap_read(data->regmap, RADIO_SCRATCH, &val);
	if (ret)
		return 0;

	if (val == R14_SCRATCHVALUE)
		return 1;
	else
		return 0;
}

static int write_slp_mac(struct device *dev)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	char mac_slp[SLPMAC_SIZE+1];
	int ret = 0;
	long int slpaddress = 0;
	u8 regbuf[SLPMAC_SIZE/2];

	strncpy(mac_slp, SLPBANK, 2);
	strncpy(mac_slp+2, data->macwan+6, 6);
	mac_slp[SLPMAC_SIZE] = '\0';

	ret = kstrtol(mac_slp, 16, &slpaddress);
	if (ret)
	{
		dev_err(dev, "Unable to set MAC for Protocol %s\n", protocol_description[data->protocol]);
		return ret;
	}

	regbuf[3] = (int)slpaddress & 0xFF;
	regbuf[2] = ((int)slpaddress & 0xFF00) >> 8;
	regbuf[1] = ((int)slpaddress & 0xFF0000) >> 16;
	regbuf[0] = ((int)slpaddress & 0xFF000000) >> 24;

	ret = regmap_bulk_write(data->regmap, RADIO_SLPMAC0, regbuf, (SLPMAC_SIZE/2));

	return ret;
}

static int write_ews_mac(struct device *dev)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	char mac_ews[EWSMAC_SIZE+1];
	int ret = 0;
	long int ewsaddress = 0;
	u8 regbuf[EWSMAC_SIZE/2];

	strncpy(mac_ews, EWSBANK, 2);
	strncpy(mac_ews+2, data->macwan+8, 4);
	mac_ews[EWSMAC_SIZE] = '\0';

	ret = kstrtol(mac_ews, 16, &ewsaddress);
	if (ret)
	{
		dev_err(dev, "Unable to set MAC for Protocol %s\n", protocol_description[data->protocol]);
		return ret;
	}

	regbuf[2] = (int)ewsaddress & 0xFF;
	regbuf[1] = ((int)ewsaddress & 0xFF00) >> 8;
	regbuf[0] = ((int)ewsaddress & 0xFF0000) >> 16;

	ret = regmap_bulk_write(data->regmap, RADIO_EWS_MAC0, regbuf, (EWSMAC_SIZE/2));

	return ret;
}

static void define_radiostate(struct device *dev)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	int lifetest = radio_lifetest(dev);
	int statepin;
	unsigned int val;

	if (data->gpio_state == NULL)
		// no state pin, assume result the same as the lifetest
		statepin = lifetest;
	else
		// more accurate is to get value of the statepin
		statepin = gpiod_get_value(data->gpio_state);

	//dev_info(dev, "%s: lifetest:%d, statepin:%d, assembled:%d\n", __func__, lifetest, statepin, data->assembled);

	// hammer while system is booting and the mcu is not responding
	if (data->assembled && !statepin && (data->poll_cycles < 4) && (data->state == S_BOOTSYSTEM)) {
		dev_info(dev, "%s: hardware reset %d\n", __func__, data->poll_cycles);
		hardreset(dev);
		return;
	}

	if (data->assembled == 0)
		data->state = S_NORADIO;
	else if (!lifetest && !statepin)
		data->state = S_BOOTLOADER;
	else if ((!lifetest && statepin) || (lifetest && !statepin))
		data->state = S_ERROR;
	else {
		regmap_read(data->regmap, RADIO_BOOT_PWR, &val);
		if (val & R0_SELFTEST)
			data->state = S_SELFTEST;
		else
			data->state = S_OPERATIONAL;
	}

	if (data->state == S_OPERATIONAL) {
		regmap_read(data->regmap, RADIO_FWVERSION, &data->swversion);
		regmap_read(data->regmap, RADIO_ACTIVE_PROT, &val);
		if (val & R2B1_SLP)
			data->protocol = P_SLP;
		else if (val & R2B2_WMBUS)
			data->protocol = P_WMBUS;
		else if (val & R2B3_EASYWAVE)
			data->protocol = P_EASYWAVE;
		else if (val & R2B5_EASYWAVESHARP)
			data->protocol = P_EASYWAVESHARP;
		else if (val & R2B0_TRANSPARENT)
			data->protocol = P_TRANSPARENT;
		else
			data->protocol = P_UNSUPPORTED;
	}

	// baudrate setting
	if (data->protocol == P_EASYWAVE && data->state == S_OPERATIONAL)
		regmap_write(data->regmap, RADIO_UART, R13_UART57K6);
	else if (data->protocol == P_EASYWAVESHARP && data->state == S_OPERATIONAL)
		regmap_write(data->regmap, RADIO_UART, R13_UART115K2);
	else if (data->protocol == P_WMBUS && data->state == S_OPERATIONAL)
		regmap_write(data->regmap, RADIO_UART, R13_UART2400E1);
	else if (data->protocol == P_SLP && data->state == S_OPERATIONAL)
		regmap_write(data->regmap, RADIO_UART, R13_UART9600);
	else
		regmap_write(data->regmap, RADIO_UART, R13_UART115K2);
	// After writing uart settings, the radio firmware needs some time ...
	msleep(1);

	// mac setting
	if (data->protocol == P_SLP && data->state == S_OPERATIONAL)
		write_slp_mac(dev);
	else if (data->protocol == P_EASYWAVESHARP && data->state == S_OPERATIONAL)
		write_ews_mac(dev);

	talk_to_udev(data->dev);

	dev_info(dev, "Radio %d mode=%s protocol=%s\n", data->radio,
			state_description[data->state], protocol_description[data->protocol]);
}

static void i2c_work_handler(struct work_struct *work)
{
	struct smartradio_data *data = container_of(work, struct smartradio_data, dwork.work);

	define_radiostate(data->dev);

	if (data->state >= S_SELFTEST) {
		talk_to_udev(data->dev);
		return;
	}

	if (data->poll_cycles >= MAX_POLL_CYCLES) {
		talk_to_udev(data->dev);
		return;
	}

	schedule_delayed_work(&data->dwork, MIN_POLL_PERIOD*HZ*data->poll_cycles*data->poll_cycles);

	data->poll_cycles++;
}

static void hardreset(struct device *dev)
{
	struct smartradio_data *data = dev_get_drvdata(dev);

	if (data->gpio_reset == NULL)
		return;

	// 1 means active, not level on 3V3!
	gpiod_set_value(data->gpio_reset, 0);
	gpiod_set_value(data->gpio_reset, 1);
	msleep(1);
	gpiod_set_value(data->gpio_reset, 0);
}

static void activate_bootloader(struct device *dev, unsigned int value)
{
	struct smartradio_data *data = dev_get_drvdata(dev);

	switch (value) {
		case 1:
			regmap_write(data->regmap, RADIO_BOOT_PWR, R0_BOOTLOADER);
			break;
		case 2:
			/* hard reset does not allow saving slp frame counters !!! */
			hardreset(dev);
			break;
		case 3:
			// radio on
			if (data->state == S_RADIOFF) {
				gpiod_set_value(data->gpio_reset, 0);
				break;
			} else {
				return;
			}
		case 4:
			// radio off
			gpiod_set_value(data->gpio_reset, 1);
			data->state = S_RADIOFF;
			return;
		default:
			return;
	}

	regmap_write(data->regmap, RADIO_UART, R13_UART115K2);

	data->state = S_BOOTLOADER;
	data->poll_cycles = 1;

	talk_to_udev(data->dev);

	cancel_delayed_work(&data->dwork);
	schedule_delayed_work(&data->dwork, MIN_POLL_PERIOD*HZ);
}

static void toggle_selftest(struct device *dev, unsigned int value)
{
	struct smartradio_data *data = dev_get_drvdata(dev);

	if (value == 0) {
		data->state = S_OPERATIONAL;
		regmap_write(data->regmap, RADIO_BOOT_PWR, R0_NORMALMODE);
	} else if (value == 1) {
		data->state = S_SELFTEST;
		regmap_write(data->regmap, RADIO_BOOT_PWR, R0_SELFTEST);
	}

	talk_to_udev(data->dev);
}

static void talk_to_udev(struct device *dev)
{
	struct smartradio_data *data = dev_get_drvdata(dev);

	char env_radio[DESC_SIZE+10];
	char env_protocol[DESC_SIZE+10];
	char env_state[DESC_SIZE+10];
	char env_tty[DESC_SIZE+10];
	char *envp[] = { env_radio, env_protocol, env_state, env_tty, NULL };

	sprintf(env_radio, "RADIO=%d", data->radio);
	sprintf(env_protocol, "PROTOCOL=%s", protocol_description[data->protocol]);
	sprintf(env_state, "STATE=%s", state_description[data->state]);
	sprintf(env_tty, "TTY=%s", data->tty);
	kobject_uevent_env(&dev->kobj, KOBJ_CHANGE, envp);
}



static ssize_t string_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);

	if (!strcmp(attr->attr.name, "protocol"))
		sprintf(buf, "%s\n", protocol_description[data->protocol]);
	else if (!strcmp(attr->attr.name, "radiostate"))
		sprintf(buf, "%s\n", state_description[data->state]);
	else if (!strcmp(attr->attr.name, "swversion"))
		sprintf(buf, "%d.%d", (data->swversion & 0x000000F0)  >> 4, (data->swversion & 0x0000000F));
	else
		return -EINVAL;

	return strlen(buf);
}

static ssize_t capabilities_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	int capabilities = 0;

	regmap_read(data->regmap, RADIO_CAPABILITIES, &capabilities);

	strcpy(buf, "");

	if (capabilities & R2B1_SLP) {
		strcat(buf, protocol_description[P_SLP]);
		strcat(buf, " ");
	}
	if (capabilities & R2B2_WMBUS) {
		strcat(buf, protocol_description[P_WMBUS]);
		strcat(buf, " ");
	}
	if (capabilities & R2B3_EASYWAVE) {
		strcat(buf, protocol_description[P_EASYWAVE]);
		strcat(buf, " ");
	}
	if (capabilities & R2B5_EASYWAVESHARP) {
		strcat(buf, protocol_description[P_EASYWAVESHARP]);
		strcat(buf, " ");
	}
	if (capabilities & R2B0_TRANSPARENT) {
		strcat(buf, protocol_description[P_TRANSPARENT]);
		strcat(buf, " ");
	}
	strcat(buf, "\n");

	return strlen(buf);
}

static ssize_t protocol_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	unsigned int regval = 0;
	char input_string[DESC_SIZE];
	int capabilities = 0;

	if (count < 2 || count >= DESC_SIZE)
		return -EINVAL;

	if (data->state == S_NORADIO)
		return count;

	strcpy(input_string, buf);
	if (count > 0 && input_string[count-1] == '\n')
		input_string[count-1] = 0;

	regmap_read(data->regmap, RADIO_CAPABILITIES, &capabilities);

	if (!strcmp(input_string, protocol_description[P_SLP])) {
		data->protocol = P_SLP;
		regval = R2B1_SLP;
	} else if (!strcmp(input_string, protocol_description[P_WMBUS])) {
		data->protocol = P_WMBUS;
		regval = R2B2_WMBUS;
	} else if (!strcmp(input_string, protocol_description[P_EASYWAVE])) {
		data->protocol = P_EASYWAVE;
		regval = R2B3_EASYWAVE;
	} else if (!strcmp(input_string, protocol_description[P_EASYWAVESHARP])) {
		data->protocol = P_EASYWAVESHARP;
		regval = R2B5_EASYWAVESHARP;
	} else if (!strcmp(input_string, protocol_description[P_TRANSPARENT])) {
		data->protocol = P_TRANSPARENT;
		regval = R2B0_TRANSPARENT;
	} else {
		return -EINVAL;
	}

	if (~capabilities & regval) {
		dev_err(dev, "Protocol %s not supported by the firmware\n", protocol_description[data->protocol]);
		data->protocol = P_UNSUPPORTED;
		return -EINVAL;
	}

	// switch off selftest
	toggle_selftest(dev, 0);
	regmap_write(data->regmap, RADIO_ACTIVE_PROT, regval);

	// immediate execution
	cancel_delayed_work(&data->dwork);
	schedule_delayed_work(&data->dwork, 0);

	return count;
}

static ssize_t hexvalue_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	unsigned int val;

	if (data->state < S_SELFTEST) {
		sprintf(buf, "0\n");
		return strlen(buf) + 1;
	}

	if (!strcmp(attr->attr.name, "selftest")) {
		regmap_read(data->regmap, RADIO_BOOT_PWR, &val);
		if (val & R0_SELFTEST)
			val = 1;
		else
			val = 0;
	} else
		return -EINVAL;

	return sprintf(buf, "%02X\n", (val & 0xFF));
}

static ssize_t hexvalue_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	long int value = 0;
	int err = 0;

	if (count < 0 || count > 3)
		return -EINVAL;

	err = kstrtol(buf, 16, &value);

	if (err < 0)
		return err;

	if (value > 0xff)
		return -EINVAL;

	if (!strcmp(attr->attr.name, "bootloader"))
		activate_bootloader(dev, (unsigned int)value);
	else if (!strcmp(attr->attr.name, "selftest"))
		toggle_selftest(dev, (unsigned int)value);
	else
		return -EINVAL;

	return count;
}

static ssize_t slpmasterkey_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	static char key_in[SLPMASTERKEY_SIZE] = "";
	u8 regbuf[SLPMASTERKEY_SIZE/2];
	int i, ret = 0;

	if (count != (SLPMASTERKEY_SIZE+1))
		return -EINVAL;

	for (i = 0; i < SLPMASTERKEY_SIZE; i++) {
		u8 c = toupper(buf[i]);
		if (isxdigit(c))
			key_in[i] = xdigit_to_i(c);
		else
			return -EINVAL;
	}

	for (i = 0; i < (SLPMASTERKEY_SIZE/2); i++)
		regbuf[i] = key_in[i*2] << 4 | key_in[(i*2)+1];

	ret = regmap_bulk_write(data->regmap, RADIO_SLP_MASTER0, regbuf, (SLPMASTERKEY_SIZE/2));
	if (ret < 0) {
		dev_err(dev, "Unable to write masterkey\n");
		return -EINVAL;
	}

	return count;
}

static ssize_t reg_addr_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);

	return sprintf(buf, "%02X\n", data->i2c_reg);
}

static ssize_t reg_val_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	unsigned int err;
	unsigned int value;

	err = regmap_read(data->regmap, data->i2c_reg, &value);
	if (err) {
		dev_err(dev, "Unable to read register %d, err=%d\n", data->i2c_reg, err);
		return 0;
	}

	return sprintf(buf, "%02X\n", value);
}

static ssize_t reg_addr_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	long int din = 0;
	int err = 0;

	if (count < 0 || count > 3)
		return -EINVAL;

	err = kstrtol(buf, 16, &din);

	if (err < 0)
		return err;

	if (din > RADIO_REGISTERS)
		return -EINVAL;

	data->i2c_reg = din;

	return count;
}

static ssize_t reg_val_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	long int value = 0;
	int err = 0;

	if (count < 0 || count > 3)
		return -EINVAL;

	err = kstrtol(buf, 16, &value);

	if (err < 0)
		return err;

	if (value > 0xff)
		return -EINVAL;

	err = regmap_write(data->regmap, data->i2c_reg, (unsigned int)value);
	if (err) {
		dev_err(dev, "Unable to write register %d, err=%d\n", data->i2c_reg, err);
		return 0;
	}

	return count;
}

static ssize_t rssi_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	int val = 0, raw = 0;

	regmap_read(data->regmap, RADIO_RSSI, &raw);

	// convert 2's-complement to signed int
	if (raw & 0x80)
		// negative value
		val = raw | ~((1 << 8) - 1);
	else
		val = raw;

	return sprintf(buf, "%+d", val);
}

static ssize_t lqi_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	unsigned int val = 0;

	regmap_read(data->regmap, RADIO_LQI, &val);
	val &= 0x7F;

	if (val)
		sprintf(buf, "%d", val);
	else
		sprintf(buf, "-");

	return strlen(buf);
}

static DEVICE_ATTR(protocol, S_IWUSR | S_IRUGO, string_show, protocol_store);
static DEVICE_ATTR(radiostate, S_IRUGO, string_show, NULL);
static DEVICE_ATTR(capabilities, S_IRUGO, capabilities_show, NULL);
static DEVICE_ATTR(swversion, S_IRUGO, string_show, NULL);
static DEVICE_ATTR(bootloader, S_IWUSR, NULL, hexvalue_store);
static DEVICE_ATTR(selftest, S_IWUSR | S_IRUGO, hexvalue_show, hexvalue_store);
static DEVICE_ATTR(slpmasterkey, S_IWUSR, NULL, slpmasterkey_store);
static DEVICE_ATTR(reg_addr, S_IWUSR | S_IRUGO, reg_addr_show, reg_addr_store);
static DEVICE_ATTR(reg_val, S_IWUSR | S_IRUGO, reg_val_show, reg_val_store);
static DEVICE_ATTR(rssi, S_IRUGO, rssi_show, NULL);
static DEVICE_ATTR(lqi, S_IRUGO, lqi_show, NULL);

static struct attribute *smartradio_attrs[] = {
	&dev_attr_protocol.attr,
	&dev_attr_radiostate.attr,
	&dev_attr_capabilities.attr,
	&dev_attr_swversion.attr,
	&dev_attr_bootloader.attr,
	&dev_attr_selftest.attr,
	&dev_attr_slpmasterkey.attr,
	&dev_attr_reg_addr.attr,
	&dev_attr_reg_val.attr,
	&dev_attr_rssi.attr,
	&dev_attr_lqi.attr,
	NULL,
};

static struct attribute_group sysfs_files = {
	.attrs = smartradio_attrs,
};

static ssize_t hwmon_show_temp(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	int ret, value;

	ret = regmap_read(data->regmap, RADIO_TEMPERATURE, &value);
	if (ret < 0)
		return ret;

	return sprintf(buf, "%d\n", (value * 1000));
}

static SENSOR_DEVICE_ATTR(temp1_input, S_IRUGO, hwmon_show_temp, NULL, 0);

static struct attribute *hwmon_attrs[] = {
	&sensor_dev_attr_temp1_input.dev_attr.attr,
	NULL,
};

ATTRIBUTE_GROUPS(hwmon);

static int get_mac_from_eeprom(struct device *dev)
{
	struct smartradio_data *data = dev_get_drvdata(dev);
	char *value;
	size_t cell_len;
	struct nvmem_cell *cell;

	cell = nvmem_cell_get(dev, "macwan");
	if (IS_ERR(cell)) {
		if (PTR_ERR(cell) == -EPROBE_DEFER)
			return PTR_ERR(cell);
		return -EINVAL;
	}

	value = nvmem_cell_read(cell, &cell_len);
	nvmem_cell_put(cell);
	if (IS_ERR(value)) {
		return -EINVAL;
	}

	snprintf(data->macwan, (MAC_SIZE+1), "%s", value);
	kfree(value);

	return 0;
}

static int smartradio_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct device_node *np = client->dev.of_node;
	struct smartradio_data *data;
	struct gpio_desc *gpio_sku;
	const char *of_string;
	int ret;

	data = devm_kzalloc(&client->dev, sizeof(*data), GFP_KERNEL);
	if (!data) {
		dev_err(&client->dev, "Malloc failed\n");
		return -ENOMEM;
	}

	data->client = client;
	data->dev = &client->dev;
	i2c_set_clientdata(client, data);

	if (client->addr == RADIO1_ADDR)
		data->radio = RADIO1;
	else if (client->addr == RADIO2_ADDR)
		data->radio = RADIO2;
	else {
		dev_err(&client->dev, "No radio I2C address match\n");
		ret = -ENODEV;
		goto jumpout;
	}

	// radio 1 is always assembled
	if (data->radio == RADIO1)
		data->assembled = 1;

	// radio 2: define presence via gpio pin
	if (data->radio == RADIO2) {
		gpio_sku = devm_gpiod_get(&client->dev, "sku", GPIOD_IN);
		if (!IS_ERR(gpio_sku))
			data->assembled = gpiod_get_value(gpio_sku);
	}

	// reset gpio pin is optional
	data->gpio_reset = devm_gpiod_get(&client->dev, "reset", GPIOD_OUT_LOW);
	if (IS_ERR(data->gpio_reset)) {
		data->gpio_reset = NULL;
		dev_info(&client->dev, "Cannot get reset-gpio, switch to SW reset\n");
	}

	// state gpio pin is not strictly necessary, but provides accurate state probing
	data->gpio_state = devm_gpiod_get(&client->dev, "state", GPIOD_IN);
	if (IS_ERR(data->gpio_state)) {
		data->gpio_state = NULL;
		dev_info(&client->dev, "Cannot get state-gpio\n");
	} else
		gpiod_export(data->gpio_state, true);

	data->regmap = devm_regmap_init_i2c(data->client, &radio_regmap_config);
	if (IS_ERR(data->regmap)) {
		ret = PTR_ERR(data->regmap);
		dev_err(&client->dev, "regmap init failed, err %d\n", ret);
		goto jumpout;
	}

	ret = sysfs_create_group(&client->dev.kobj, &sysfs_files);
	if (ret) {
		dev_err(&client->dev, "failed to create sysfs\n");
		goto jumpout;
	}

	//  creating platform independent link under /sys/kernel/emperor/
	sprintf(data->sysfslink, "radio%d", data->radio);
	ret = sysfs_create_link(emperor_kobj, &client->dev.kobj, data->sysfslink);
	if (ret) {
		dev_err(&client->dev, "failed to link sysfs\n");
		goto jumpout;
	}

	ret = of_property_read_string(np, "macwan", &of_string);
	if (!ret)
		strncpy(data->macwan, of_string, MAC_SIZE);
	else
		// the preferred way: read value from eeprom
		if (get_mac_from_eeprom(&client->dev) == -EPROBE_DEFER)
			return ret;

	ret = of_property_read_string(np, "tty", &of_string);
	if (!ret)
		strcpy(data->tty, of_string);
	else
		strcpy(data->tty, "");

	data->hwmon_dev = devm_hwmon_device_register_with_groups(&client->dev, client->name, data, hwmon_groups);
	if (IS_ERR(data->hwmon_dev))
		dev_err(&client->dev, "failed to create hwmon device\n");

	data->state = S_BOOTSYSTEM;
	data->protocol = P_UNSUPPORTED;
	data->i2c_reg = RADIO_FWVERSION;
	data->poll_cycles = 1;
	data->swversion = 0;

	if (data->assembled == 1) {
		INIT_DELAYED_WORK(&data->dwork, i2c_work_handler);
		// perform the worker
		schedule_delayed_work(&data->dwork, MIN_POLL_PERIOD*HZ);
	} else
		dev_info(&client->dev, "Radio %d not assembled\n", data->radio);

	return 0;

jumpout:
	kfree(data);
	return ret;
}

static int smartradio_remove(struct i2c_client *client)
{
	struct smartradio_data *data = i2c_get_clientdata(client);

	cancel_delayed_work_sync(&data->dwork);

	if (data->hwmon_dev)
		hwmon_device_unregister(data->hwmon_dev);

	sysfs_remove_link(emperor_kobj, data->sysfslink);
	sysfs_remove_group(&client->dev.kobj, &sysfs_files);

	dev_info(&client->dev, "driver successfully removed\n");

	return 0;
}

static const struct i2c_device_id smartradio_id[] = {
	{ DRIVER_NAME, 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, smartradio_id);

static const struct of_device_id of_smartradio_match[] = {
	{ .compatible = DRIVER_NAME, },
	{},
};
MODULE_DEVICE_TABLE(of, of_smartradio_match);

static struct i2c_driver smartradio_driver = {
	.driver = {
		.name	= DRIVER_NAME,
		.of_match_table = of_match_ptr(of_smartradio_match),
	},
	.probe		= smartradio_probe,
	.remove		= smartradio_remove,
	.id_table	= smartradio_id,
};
module_i2c_driver(smartradio_driver);

MODULE_AUTHOR("Wim De Witte <de.witte.wim (at) gmail.com>");
MODULE_DESCRIPTION("Smartbox radio driver");
MODULE_LICENSE("GPL");
