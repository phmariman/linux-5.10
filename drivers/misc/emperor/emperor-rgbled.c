/*
 * RGB led driver for CEHAM
 *
 * Copyright (C) 2013 Wim De Witte <de.witte.wim (at) gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
 */

/*
 * emperor-rgbled.c - GPIO connected RGB led, used on emperor boards.
 * With this driver, all kind of effects can be controlled for the RGB led.
 * 
 * This driver exports a few files in /sys/kernel/emperor/rgbled
 *    mode - set up the led mode, see function rgbmode_store;
 *           if the mode is manual, following files can be set:
 *    color - 3-bit value for the color: bit2=blue, bit1=red, bit0=green
 *    blink - 3-bit value of which colors will blink (decoding as color)
 *    interval - blinking period in 10msec (100 corresponds with 1 sec on / 1 sec off)
 *    brightness - led intensity between 1 and 100
 *    ramp - turn on/off the hardware ramping (smooth on/off)
 * 
 */
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/kobject.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/sysfs.h>
#include <linux/gpio.h>
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/clk.h>
#include <linux/pwm.h>

#define MODE_SIZE	64
#define BLINKINTERVAL	150

#define BIT_NONE	0
#define BIT_GREEN	1
#define BIT_RED		2
#define BIT_BLUE	4

#define OFF		0
#define ON		1

#define	DRIVER_NAME	"emperor-rgbled"
#define MAX_BRIGHTNESS	100
#define MIN_BRIGHTNESS	1

extern struct kobject *emperor_kobj;


typedef enum {
	MODE_OFF = 0,	// Led is switched off
	MODE_OK,	// The gate is polling with the core, no issues on gate level
	MODE_ERROR,	// Severe error in the gate that prevents normal functionality
	MODE_NO_INT,	// There is no Internet connection
	MODE_NO_CORE,	// The gateway can not poll to the core
	MODE_START,	// Gate is starting up
	MODE_UPGRADE,	// Gate is upgrading
	MODE_MANUAL,	// Manual led control
	MODE_START_SBPLUS,	// Smarbox+ is starting up
} LEDMODE;

typedef enum {
	LED_GREEN = 0,
	LED_RED,
	LED_BLUE,
	LED_RAMP,
	LED_COUNT
} LED_ID;

struct rgb_data {
	struct device_node *dn;
	rwlock_t lock;
	char mode_string[MODE_SIZE];
	LEDMODE mode;
	int blinkinterval;
	struct timer_list blink_timer;
	struct pwm_device *pwm;
	unsigned int scale;
	unsigned int period;
	int blinkmode;
	int manualcolor;
	int manualblink;
	int ramp;
	int brightness;
	unsigned leds[LED_COUNT];
};

static void set_ramp(struct rgb_data *data, int ramp)
{
	if (ramp > 1)
		ramp = ON;

	data->ramp = ramp;
	gpio_direction_output(data->leds[LED_RAMP], data->ramp);
}

static void set_brightness(struct rgb_data *data, int brightness)
{
	int duty_cycle;

	if (brightness > MAX_BRIGHTNESS)
		brightness = MAX_BRIGHTNESS;

	if (brightness < MIN_BRIGHTNESS)
		brightness = MIN_BRIGHTNESS;

	data->brightness = brightness;

	duty_cycle = (brightness * (data->period - data->brightness) / data->scale) + data->brightness;
	pwm_config(data->pwm, duty_cycle, data->period);
	pwm_enable(data->pwm);

	pr_debug("%s: brightness=%d, duty_cycle=%d, period=%d\n", DRIVER_NAME, brightness, duty_cycle, data->period);
}

static int setledmode(struct rgb_data *data)
{
	data->blinkmode = BIT_NONE;

	// ramp is default on
	set_ramp(data, ON);
	// set brightness default to max
	if (data->mode == MODE_OK)
		set_brightness(data, 30);
	else
		set_brightness(data, MAX_BRIGHTNESS);

	switch (data->mode) {
	case MODE_OFF:
	default:
		gpio_direction_output(data->leds[LED_GREEN], 0);
		gpio_direction_output(data->leds[LED_RED], 0);
		gpio_direction_output(data->leds[LED_BLUE], 0);
		break;
	case MODE_OK:
		gpio_direction_output(data->leds[LED_GREEN], 1);
		gpio_direction_output(data->leds[LED_RED], 0);
		gpio_direction_output(data->leds[LED_BLUE], 0);
		break;
	case MODE_ERROR:
		data->blinkmode |= BIT_RED;
		gpio_direction_output(data->leds[LED_GREEN], 0);
		gpio_direction_output(data->leds[LED_RED], 1);
		gpio_direction_output(data->leds[LED_BLUE], 0);
		break;
	case MODE_START:
	case MODE_UPGRADE:
		data->blinkmode |= BIT_GREEN;
		gpio_direction_output(data->leds[LED_GREEN], 0);
		gpio_direction_output(data->leds[LED_RED], 0);
		gpio_direction_output(data->leds[LED_BLUE], 0);
		break;
	case MODE_START_SBPLUS:
		data->blinkmode |= BIT_BLUE;
		gpio_direction_output(data->leds[LED_GREEN], 0);
		gpio_direction_output(data->leds[LED_RED], 0);
		gpio_direction_output(data->leds[LED_BLUE], 0);
		break;
	case MODE_NO_INT:
		data->blinkmode |= BIT_GREEN;
		data->blinkmode |= BIT_RED;
		// when 2 colors are ramping simultaneous, color is not uniform
		set_ramp(data, OFF);
		gpio_direction_output(data->leds[LED_GREEN], 0);
		gpio_direction_output(data->leds[LED_RED], 0);
		gpio_direction_output(data->leds[LED_BLUE], 0);
		break;
	case MODE_NO_CORE:
		gpio_direction_output(data->leds[LED_GREEN], 1);
		gpio_direction_output(data->leds[LED_RED], 1);
		gpio_direction_output(data->leds[LED_BLUE], 0);
		break;
	case MODE_MANUAL:
		set_ramp(data, OFF);
		data->blinkmode = data->manualblink;
		gpio_direction_output(data->leds[LED_GREEN], data->manualcolor & BIT_GREEN);
		gpio_direction_output(data->leds[LED_BLUE], data->manualcolor & BIT_BLUE);
		gpio_direction_output(data->leds[LED_RED], data->manualcolor & BIT_RED);
		break;
	}
	
	return 0;
}

static void led_timer_function(struct timer_list *t)
{	
	struct rgb_data *data_p = from_timer(data_p, t, blink_timer);

	if (data_p->blinkmode & BIT_GREEN)
		gpio_direction_output(data_p->leds[LED_GREEN], !gpio_get_value(data_p->leds[LED_GREEN]));

	if (data_p->blinkmode & BIT_RED)
		gpio_direction_output(data_p->leds[LED_RED], !gpio_get_value(data_p->leds[LED_RED]));
	
	if (data_p->blinkmode & BIT_BLUE)
		gpio_direction_output(data_p->leds[LED_BLUE], !gpio_get_value(data_p->leds[LED_BLUE]));
	
	mod_timer(&data_p->blink_timer, jiffies + data_p->blinkinterval); /* restart the timer */
}

static ssize_t rgbmode_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct rgb_data *data = dev_get_drvdata(dev);

	LEDMODE oldmode = data->mode;

	if (count < 0 || count >= MODE_SIZE)
		return -EINVAL;

	write_lock(&data->lock);

	strcpy(data->mode_string, buf);
	if (count > 0 && data->mode_string[count-1] == '\n')
		data->mode_string[count-1] = 0;

	if (data->mode_string[0] != 0) {
		if (!strcmp(data->mode_string, "off"))
			data->mode = MODE_OFF;
		else if (!strcmp(data->mode_string, "ok"))
			data->mode = MODE_OK;
		else if (!strcmp(data->mode_string, "error"))
			data->mode = MODE_ERROR;
		else if (!strcmp(data->mode_string, "nointernet"))
			data->mode = MODE_NO_INT;
		else if (!strcmp(data->mode_string, "nobackend"))
			data->mode = MODE_NO_CORE;
		else if (!strcmp(data->mode_string, "start"))
			data->mode = MODE_START;
		else if (!strcmp(data->mode_string, "upgrade"))
			data->mode = MODE_UPGRADE;
		else if (!strcmp(data->mode_string, "manual"))
			data->mode = MODE_MANUAL;
		else {
			data->mode = MODE_OFF;
			strcpy(data->mode_string, "off");
		}
	}

	write_unlock(&data->lock);

	if (data->mode != MODE_MANUAL) {
		data->blinkinterval = BLINKINTERVAL;
	}

	if (oldmode != data->mode)
		setledmode(data);

	return count;
}

static ssize_t rgbcolor_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int mycolor;
	int oldcolor;
	int ret;

	struct rgb_data *data = dev_get_drvdata(dev);

	oldcolor = data->manualcolor;

	if (count < 0 || count > 2)
		return -EINVAL;

	write_lock(&data->lock);

	ret = sscanf(buf, "%d", &mycolor);
	if (ret < 1) {
		pr_err("%s: couldn't read color value\n", DRIVER_NAME);
		return -EINVAL;
	}

	write_unlock(&data->lock);

	if (mycolor > 7)
		mycolor = 7;

	data->manualcolor = mycolor;

	if (oldcolor != data->manualcolor)
		setledmode(data);

	return count;
}

static ssize_t rgbblink_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int myblink;
	int oldblink;
	int ret;

	struct rgb_data *data = dev_get_drvdata(dev);

	oldblink = data->manualblink;

	if (count < 0 || count > 2)
		return -EINVAL;

	write_lock(&data->lock);

	ret = sscanf(buf, "%d", &myblink);
	if (ret < 1) {
		pr_err("%s: couldn't read blink value\n", DRIVER_NAME);
		return -EINVAL;
	}

	write_unlock(&data->lock);

	if (myblink > 7)
		myblink = 7;

	data->manualblink = myblink;

	if (oldblink != data->manualblink)
		setledmode(data);

	return count;
}

static ssize_t rgbramp_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int myramp;
	int ret;

	struct rgb_data *data = dev_get_drvdata(dev);

	if (count < 0 || count > 2)
		return -EINVAL;

	write_lock(&data->lock);

	ret = sscanf(buf, "%d", &myramp);
	if (ret < 1) {
		pr_err("%s: couldn't read ramp value\n", DRIVER_NAME);
		return -EINVAL;
	}

	write_unlock(&data->lock);

	set_ramp(data, myramp);

	return count;
}

static ssize_t rgbinterval_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int myinterval;
	int ret;

	struct rgb_data *data = dev_get_drvdata(dev);

	if (count < 0 || count > 5)
		return -EINVAL;

	write_lock(&data->lock);

	ret = sscanf(buf, "%d", &myinterval);
	if (ret < 1) {
		pr_err("%s: couldn't read interval value\n", DRIVER_NAME);
		return -EINVAL;
	}

	write_unlock(&data->lock);

	// more than 15 seconds is irrelevant
	if (myinterval > BLINKINTERVAL*10)
		myinterval = BLINKINTERVAL*10;

	// if ramp is on, and frequency is to low; capacitors cannot charge and the led will stay off
	if ((data->ramp == ON) && (myinterval < 50))
		myinterval = 50;

	data->blinkinterval = myinterval;

	return count;
}

static ssize_t rgbbrightness_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int mybrightness;
	int ret;

	struct rgb_data *data = dev_get_drvdata(dev);

	if (count < 0 || count > 6)
		return -EINVAL;

	write_lock(&data->lock);

	ret = sscanf(buf, "%d", &mybrightness);
	if (ret < 1) {
		dev_info(dev, "couldn't read brightness value\n");
		return -EINVAL;
	}

	write_unlock(&data->lock);

	set_brightness(data, mybrightness);

	return count;
}

static ssize_t rgbmode_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct rgb_data *data = dev_get_drvdata(dev);

	read_lock(&data->lock);
	sprintf(buf, "%s\n", data->mode_string);
	read_unlock(&data->lock);

	return strlen(buf) + 1;
}

static ssize_t rgbcolor_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct rgb_data *data = dev_get_drvdata(dev);

	read_lock(&data->lock);
	sprintf(buf, "%d\n", data->manualcolor);
	read_unlock(&data->lock);

	return strlen(buf) + 1;
}

static ssize_t rgbblink_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct rgb_data *data = dev_get_drvdata(dev);

	read_lock(&data->lock);
	sprintf(buf, "%d\n", data->manualblink);
	read_unlock(&data->lock);

	return strlen(buf) + 1;
}

static ssize_t rgbramp_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct rgb_data *data = dev_get_drvdata(dev);

	read_lock(&data->lock);
	sprintf(buf, "%d\n", data->ramp);
	read_unlock(&data->lock);

	return strlen(buf) + 1;
}

static ssize_t rgbinterval_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct rgb_data *data = dev_get_drvdata(dev);

	read_lock(&data->lock);
	sprintf(buf, "%d\n", data->blinkinterval);
	read_unlock(&data->lock);

	return strlen(buf) + 1;
}

static ssize_t rgbbrightness_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct rgb_data *data = dev_get_drvdata(dev);

	read_lock(&data->lock);
	sprintf(buf, "%d\n", data->brightness);
	read_unlock(&data->lock);

	return strlen(buf) + 1;
}

static DEVICE_ATTR(mode, S_IWUSR | S_IRUGO, rgbmode_show, rgbmode_store);
static DEVICE_ATTR(color, S_IWUSR | S_IRUGO, rgbcolor_show, rgbcolor_store);
static DEVICE_ATTR(blink, S_IWUSR | S_IRUGO, rgbblink_show, rgbblink_store);
static DEVICE_ATTR(ramp, S_IWUSR | S_IRUGO, rgbramp_show, rgbramp_store);
static DEVICE_ATTR(interval, S_IWUSR | S_IRUGO, rgbinterval_show, rgbinterval_store);
static DEVICE_ATTR(brightness, S_IWUSR | S_IRUGO, rgbbrightness_show, rgbbrightness_store);

static struct attribute *rgbled_attrs[] = {
	&dev_attr_mode.attr,
	&dev_attr_color.attr,
	&dev_attr_blink.attr,
	&dev_attr_ramp.attr,
	&dev_attr_interval.attr,
	&dev_attr_brightness.attr,
	NULL,
};

static struct attribute_group rgbled_sysfs_files = {
	.attrs = rgbled_attrs,
};

static void initialize_rgb_data(struct rgb_data *data, struct device_node *dn)
{
	data->mode = MODE_START_SBPLUS;
	data->blinkmode = BIT_NONE;
	data->manualcolor = BIT_NONE;
	data->manualblink = BIT_NONE;
	data->brightness = MAX_BRIGHTNESS;
	sprintf(data->mode_string, "start");
	data->blinkinterval = BLINKINTERVAL;
	data->dn = dn;
}

static void initialize_rgb_timer(struct rgb_data *data)
{
	timer_setup(&data->blink_timer, led_timer_function, 0);
	mod_timer(&data->blink_timer, jiffies + data->blinkinterval);
}

static void stop_rgb_timer(struct rgb_data *data)
{
	del_timer_sync(&data->blink_timer);	// stop the blink timer
}

static void stop_rgb_pwm(struct rgb_data *data)
{
	pwm_config(data->pwm, 0, data->period);
	pwm_disable(data->pwm);
}

static int setup_gpio_pin(struct rgb_data *data, LED_ID led_id, const char *label)
{
	int status = 0;
	int gpio = 0;

	gpio = of_get_named_gpio(data->dn, label, 0);
	if (gpio < 0) {
		pr_err("%s: Failed to read %s\n", DRIVER_NAME, label);
		return -1;
	}
	status = gpio_request(gpio, label);
	if (status < 0) {
		pr_err("%s: Failed to request gpio %d for %s\n", DRIVER_NAME, gpio, label);
		return -1;
	}
	data->leds[led_id] = gpio;
	return 0;
}

static int initialize_rgb_gpio(struct rgb_data *data)
{
	int status = 0;

	status += setup_gpio_pin(data, LED_GREEN, "led_green");
	status += setup_gpio_pin(data, LED_RED, "led_red");
	status += setup_gpio_pin(data, LED_BLUE, "led_blue");
	status += setup_gpio_pin(data, LED_RAMP, "led_ramp");

	return status;
}

static int free_rgb_gpio(struct rgb_data *data)
{
	gpio_free(data->leds[LED_GREEN]);
	gpio_free(data->leds[LED_RED]);
	gpio_free(data->leds[LED_BLUE]);
	gpio_free(data->leds[LED_RAMP]);

	return 0;
}

static int probe_pwm(struct platform_device *pdev, struct rgb_data *data)
{
	struct pwm_args pargs;

	data->pwm = devm_pwm_get(&pdev->dev, NULL);
	if (IS_ERR(data->pwm) && PTR_ERR(data->pwm) != -EPROBE_DEFER && !pdev->dev.of_node) {
		dev_err(&pdev->dev, "unable to request PWM\n");
		return PTR_ERR(data->pwm);
	}

	dev_dbg(&pdev->dev, "got PWM for rgbled\n");

	pwm_get_args(data->pwm, &pargs);
	data->period = pargs.period;
	data->scale = MAX_BRIGHTNESS;
	data->brightness = MAX_BRIGHTNESS * (data->period / data->scale); // initial brightness at maximum

	set_brightness(data, MAX_BRIGHTNESS);

	return 0;
}

static int rgbled_probe(struct platform_device *pdev)
{
	int retval = -EINVAL;
	struct device_node *dn = pdev->dev.of_node;
	struct rgb_data *data;

	data = kzalloc(sizeof(struct rgb_data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	initialize_rgb_data(data, dn);

	retval = initialize_rgb_gpio(data);
	if (retval) {
		dev_err(&pdev->dev, "failed to request gpio\n");
		kfree(data);
		return retval;
	}

	retval = probe_pwm(pdev, data);
	if (retval) {
		dev_err(&pdev->dev, "failed to request pwm\n");
		kfree(data);
		return retval;
	}

	retval = sysfs_create_group(&pdev->dev.kobj, &rgbled_sysfs_files);
	if (retval) {
		dev_err(&pdev->dev, "failed to create sysfs\n");
		kfree(data);
		return retval;
	}

	//  creating platform independent link under /sys/kernel/emperor/rgbled
	retval = sysfs_create_link(emperor_kobj, &pdev->dev.kobj, "rgbled");
	if (retval) {
		dev_err(&pdev->dev, "failed to link sysfs\n");
		kfree(data);
		return retval;
	}

	initialize_rgb_timer(data);
	platform_set_drvdata(pdev, data);
	setledmode(data);

	pr_info("%s: driver successfully probed\n", DRIVER_NAME);

	return 0;
}

static int rgbled_remove(struct platform_device *pdev)
{
	struct rgb_data *data;

	data = platform_get_drvdata(pdev);

	free_rgb_gpio(data);
	stop_rgb_timer(data);
	stop_rgb_pwm(data);

	platform_set_drvdata(pdev, NULL);
	kfree(data);

	sysfs_remove_group(&pdev->dev.kobj, &rgbled_sysfs_files);

	pr_info("%s: driver successfully removed\n", DRIVER_NAME);

	return 0;
}

static const struct of_device_id rgbled_id_table[] = {
	{
		.compatible	= DRIVER_NAME,
	},
	{}	
};
MODULE_DEVICE_TABLE(of, rgbled_id_table);

static struct platform_driver rgbled_driver = {
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = rgbled_id_table,
	},
	.probe   = rgbled_probe,
	.remove  = rgbled_remove,
};

module_platform_driver(rgbled_driver);
MODULE_AUTHOR("Wim De Witte <de.witte.wim (at) gmail.com>");
MODULE_AUTHOR("Jasper Nevens <jasper.nevens (at) linux.com>");
MODULE_DESCRIPTION("HAM RGB led driver");
MODULE_LICENSE("GPL");
