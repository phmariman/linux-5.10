/*
 * This driver exposes all eeprom data in sysfs entries.
 * All individual data items in the eeprom are represented by a sysfs file.
 * The map file of the eeprom is configured in the device-tree (see emperor-eeprom.dtsi).
 * The sysfs files are available under /sys/kernel/emperor/eeprom
 * Depending the data content - decimal, hex, ascii or ip address - input checking is performed.
 * The eeprom write protect pin is available under /sys/class/emperor/gpio/eeprom_wp (used for legacy)
 *
 */

#include <linux/ctype.h>
#include <linux/init.h>
#include <linux/kobject.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/of_gpio.h>
#include <linux/nvmem-consumer.h>
#include <linux/inet.h>
#include <linux/in.h>

#define DRIVER_NAME	"emperor-eeprom"

#ifndef false
#define false 0
#endif
#ifndef true
#define true 1
#endif

extern struct kobject *emperor_kobj;
extern int setup_gpio_pin(struct device_node *dn, const char *label, unsigned long flags);

struct eeprom_data {
	rwlock_t lock;
	int gpio_eeprom_wp;
};

typedef enum {
	IN_DEC_OUT_ASCII = 0,
	IN_HEX_OUT_ASCII,
	IN_HEX_OUT_HEX,
	IN_ASCII_OUT_ASCII,
	ERASED_CELLS
} STRINGTYPE;

#define NO_IP cpu_to_be32(INADDR_NONE)

__be32 ip_from_string(char *name)
{
	__be32 addr;
	int octets = 0;
	char *cp, *cq;

	cp = cq = name;
	while (octets < 4) {
		while (*cp >= '0' && *cp <= '9')
			cp++;
		if (cp == cq || cp - cq > 3)
			break;
		if (*cp == '.' || octets == 3)
			octets++;
		if (octets < 4)
			cp++;
		cq = cp;
	}
	if (octets == 4 && (*cp == ':' || *cp == '\0')) {
		if (*cp == ':')
			*cp++ = '\0';
		addr = in_aton(name);
		memmove(name, cp, strlen(cp) + 1);
	} else
		addr = NO_IP;

	return addr;
}

static u8 xdigit_to_i(u8 c)
{
	u8 ret;

	if (isdigit(c))
		ret = c - '0';
	else if ('A' <= c && c <= 'F')
		ret = c - 'A' + 10;
	else if ('a' <= c && c <= 'f')
		ret = c - 'a' + 10;
	else
		ret = 0xF;

	return ret;
}

static int checkstring(char *s, STRINGTYPE type)
{
	while (*s != '\0') {
		switch (type) {
		case IN_DEC_OUT_ASCII:
			if (!isdigit(*s++))
				return 0;
			break;
		case IN_HEX_OUT_ASCII:
		case IN_HEX_OUT_HEX:
			if (!isxdigit(*s++))
				return 0;
			break;
		case IN_ASCII_OUT_ASCII:
			if (!isascii(*s++))
				return 0;
			break;
		case ERASED_CELLS:
			if (*s++ == 0xFF)
				return 0;
			break;
		default:
			return 0;
		}
	}

	return 1;
}

static int eeprom_protection(struct device *dev, bool mode)
{
	struct eeprom_data *data = dev_get_drvdata(dev);

	if (!data->gpio_eeprom_wp)
		return 1;

	if (mode)
		// true: set pin high to protect write capability
		gpio_direction_output(data->gpio_eeprom_wp, 1);
	else
		gpio_direction_output(data->gpio_eeprom_wp, 0);

	return 0;
}

STRINGTYPE get_valuetype(struct device_attribute *attr)
{
	if (!strcmp(attr->attr.name, "sku") ||
			!strcmp(attr->attr.name, "hwsetting") ||
			!strcmp(attr->attr.name, "boot_testmode") ||
			!strcmp(attr->attr.name, "boot_netmode") ||
			!strcmp(attr->attr.name, "boot_chart") ||
			!strcmp(attr->attr.name, "boot_wdog"))
		return IN_HEX_OUT_HEX;

	if (!strcmp(attr->attr.name, "dateproduction") ||
			!strcmp(attr->attr.name, "lastaccess") ||
			!strcmp(attr->attr.name, "macwan") ||
			!strcmp(attr->attr.name, "maclan"))
		return IN_HEX_OUT_ASCII;

	if (!strcmp(attr->attr.name, "pwsalt") ||
			!strcmp(attr->attr.name, "pwhash") ||
			!strcmp(attr->attr.name, "https") ||
			!strcmp(attr->attr.name, "wpakey") ||
			!strcmp(attr->attr.name, "mfgrserial") ||
			!strcmp(attr->attr.name, "datablob"))
		return IN_ASCII_OUT_ASCII;

	return IN_DEC_OUT_ASCII;
}

static ssize_t string_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	size_t len;
	char *value;
	char *oldvalue;
	struct nvmem_cell *cell;
	STRINGTYPE valuetype = get_valuetype(attr);
	struct eeprom_data *data = dev_get_drvdata(dev);

	cell = nvmem_cell_get(dev, attr->attr.name);
	if (IS_ERR(cell)) {
		dev_err(dev, "failed to get nvmem cell '%s'\n", attr->attr.name);
		return -EINVAL;
	}

	read_lock(&data->lock);
	// read to define the length of cell, not interested in contents
	oldvalue = nvmem_cell_read(cell, &len);
	read_unlock(&data->lock);
	if (IS_ERR(oldvalue)) {
		nvmem_cell_put(cell);
		return -EINVAL;
	}
	kfree(oldvalue);

	// substract end-off-line from the count
	if ((count - 1) != len) {
		dev_info(dev, "invalid string length (%d) for nvmem cell '%s'\n", (count - 1), attr->attr.name);
		nvmem_cell_put(cell);
		return -EINVAL;
	}

	value = kzalloc(len, GFP_KERNEL);
	if (!value) {
		nvmem_cell_put(cell);
		return -ENOMEM;
	}

	if (valuetype == IN_HEX_OUT_HEX)
		value[0] = xdigit_to_i(buf[0]);
	else
		strncpy(value, buf, len);

	// because these settings are extremely crucial, do some extra input checking (upgraded_rfs can be '0')
	if (!strcmp(attr->attr.name, "rfs"))
		if (((buf[0] != '2') && (buf[0] != '3')) || ((buf[1] != '0') && (buf[1] != '2') && (buf[1] != '3'))) {
			dev_info(dev, "invalid value for nvmem cell '%s'\n", attr->attr.name);
			kfree(value);
			nvmem_cell_put(cell);
			return -EINVAL;
		}

	if (!checkstring(value, valuetype) && (valuetype != IN_HEX_OUT_HEX)) {
		dev_info(dev, "invalid string (%s) for nvmem cell '%s'\n", value, attr->attr.name);
		kfree(value);
		nvmem_cell_put(cell);
		return -EINVAL;
	}

	eeprom_protection(dev, false);
	write_lock(&data->lock);
	nvmem_cell_write(cell, value, len);
	write_unlock(&data->lock);
	eeprom_protection(dev, true);
	nvmem_cell_put(cell);
	kfree(value);
	return count;
}

static ssize_t string_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	char *value;
	size_t len;
	int i;
	ssize_t string_size;
	struct nvmem_cell *cell;
	struct eeprom_data *data = dev_get_drvdata(dev);
	STRINGTYPE valuetype = get_valuetype(attr);

	cell = nvmem_cell_get(dev, attr->attr.name);
	if (IS_ERR(cell)) {
		dev_err(dev, "failed to get nvmem cell %s\n", attr->attr.name);
		return -EINVAL;
	}

	read_lock(&data->lock);
	value = nvmem_cell_read(cell, &len);
	read_unlock(&data->lock);
	nvmem_cell_put(cell);
	if (IS_ERR(value)) {
		return -EINVAL;
	}

	if (valuetype == IN_HEX_OUT_HEX) {
		string_size = sprintf(buf, "%1X\n", value[0]);
	} else {
		// if complete string is 0xFF, then replace the string with F characters
		if (!checkstring(value, ERASED_CELLS)) {
			for (i = 0; i < len; i++)
				*(value + i) = 'F';
		}
		string_size = sprintf(buf, "%s\n", value);
	}
	kfree(value);

	return string_size;
}

static ssize_t ipaddr_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	size_t len;
	char *value, *oldvalue;
	__be32 addr;
	struct nvmem_cell *cell;
	struct eeprom_data *data = dev_get_drvdata(dev);
	static char ip_in[16] = "";

	cell = nvmem_cell_get(dev, attr->attr.name);
	if (IS_ERR(cell)) {
		dev_err(dev, "failed to get nvmem cell '%s'\n", attr->attr.name);
		return -EINVAL;
	}

	read_lock(&data->lock);
	// read to define the length of cell, not interested in contents
	oldvalue = nvmem_cell_read(cell, &len);
	read_unlock(&data->lock);
	if (IS_ERR(oldvalue)) {
		nvmem_cell_put(cell);
		return -EINVAL;
	}
	kfree(oldvalue);

	strlcpy(ip_in, buf, count);
	addr = ip_from_string(ip_in);

	value = kzalloc(len, GFP_KERNEL);
	if ((!value) || (addr == NO_IP)) {
		nvmem_cell_put(cell);
		return -ENOMEM;
	}

	value[0] = addr;
	value[1] = addr>>8;
	value[2] = addr>>16;
	value[3] = addr>>24;

	eeprom_protection(dev, false);
	write_lock(&data->lock);
	nvmem_cell_write(cell, value, len);
	write_unlock(&data->lock);
	eeprom_protection(dev, true);

	kfree(value);
	nvmem_cell_put(cell);

	return count;
}

static ssize_t ipaddr_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	char *value;
	size_t len;
	ssize_t string_size;
	struct nvmem_cell *cell;
	struct eeprom_data *data = dev_get_drvdata(dev);

	cell = nvmem_cell_get(dev, attr->attr.name);
	if (IS_ERR(cell)) {
		dev_err(dev, "failed to get nvmem cell %s\n", attr->attr.name);
		return -EINVAL;
	}

	read_lock(&data->lock);
	value = nvmem_cell_read(cell, &len);
	read_unlock(&data->lock);
	nvmem_cell_put(cell);
	if (IS_ERR(value)) {
		return -EINVAL;
	}
	string_size = sprintf(buf, "%d.%d.%d.%d\n", value[0], value[1], value[2], value[3]);
	kfree(value);

	return string_size;
}


static ssize_t datablob_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	size_t len;
	char *value, *oldvalue;
	struct nvmem_cell *cell;
	struct eeprom_data *data = dev_get_drvdata(dev);

	cell = nvmem_cell_get(dev, attr->attr.name);
	if (IS_ERR(cell)) {
		dev_err(dev, "failed to get nvmem cell '%s'\n", attr->attr.name);
		return -EINVAL;
	}

	read_lock(&data->lock);
	// read to define the length of cell, not interested in contents
	oldvalue = nvmem_cell_read(cell, &len);
	read_unlock(&data->lock);
	if (IS_ERR(oldvalue)) {
		nvmem_cell_put(cell);
		return -EINVAL;
	}
	kfree(oldvalue);

	if (count > len) {
		dev_info(dev, "string too long (%d) for nvmem cell '%s'\n", count, attr->attr.name);
		nvmem_cell_put(cell);
		return -EINVAL;
	}

	value = kzalloc(len, GFP_KERNEL);
	if (!value) {
		nvmem_cell_put(cell);
		return -ENOMEM;
	}

	strncpy(value, buf, len);
	eeprom_protection(dev, false);
	write_lock(&data->lock);
	nvmem_cell_write(cell, value, len);
	write_unlock(&data->lock);
	eeprom_protection(dev, true);
	nvmem_cell_put(cell);
	kfree(value);

	return count;
}
static ssize_t datablob_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	char *value;
	size_t len;
	ssize_t string_size;
	struct nvmem_cell *cell;
	struct eeprom_data *data = dev_get_drvdata(dev);

	cell = nvmem_cell_get(dev, attr->attr.name);
	if (IS_ERR(cell)) {
		dev_err(dev, "failed to get nvmem cell %s\n", attr->attr.name);
		return -EINVAL;
	}

	read_lock(&data->lock);
	value = nvmem_cell_read(cell, &len);
	read_unlock(&data->lock);
	nvmem_cell_put(cell);
	if (IS_ERR(value))
		return -EINVAL;

	string_size = snprintf(buf, len+1, "%s", value);
	kfree(value);

	return string_size;
}

static DEVICE_ATTR(rfs, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(dateproduction, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(hwsetting, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(lastaccess, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(sku, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(mfgrserial, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(fpserial, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(macwan, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(maclan, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(pwsalt, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(pwhash, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(https, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(wpakey, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(boot_testmode, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(boot_netmode, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(boot_chart, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(boot_wdog, S_IWUSR | S_IRUGO, string_show, string_store);
static DEVICE_ATTR(boot_ipaddr, S_IWUSR | S_IRUGO, ipaddr_show, ipaddr_store);
static DEVICE_ATTR(boot_server, S_IWUSR | S_IRUGO, ipaddr_show, ipaddr_store);
static DEVICE_ATTR(datablob, S_IWUSR | S_IRUGO, datablob_show, datablob_store);

static struct attribute *eeprom_attrs[] = {
	&dev_attr_rfs.attr,
	&dev_attr_dateproduction.attr,
	&dev_attr_hwsetting.attr,
	&dev_attr_lastaccess.attr,
	&dev_attr_sku.attr,
	&dev_attr_mfgrserial.attr,
	&dev_attr_fpserial.attr,
	&dev_attr_macwan.attr,
	&dev_attr_maclan.attr,
	&dev_attr_pwsalt.attr,
	&dev_attr_pwhash.attr,
	&dev_attr_https.attr,
	&dev_attr_wpakey.attr,
	&dev_attr_boot_testmode.attr,
	&dev_attr_boot_netmode.attr,
	&dev_attr_boot_chart.attr,
	&dev_attr_boot_wdog.attr,
	&dev_attr_boot_ipaddr.attr,
	&dev_attr_boot_server.attr,
	&dev_attr_datablob.attr,
	NULL,
};

static struct attribute_group sysfs_files = {
	.attrs = eeprom_attrs,
};

static const struct of_device_id emperor_eeprom_id_table[] = {
	{
		.compatible	= "emperor-eeprom",
	},
	{}
};
MODULE_DEVICE_TABLE(of, emperor_eeprom_id_table);

static int emperor_eeprom_probe(struct platform_device *pdev)
{
	int retval = 0;
	struct eeprom_data *data;
	struct device_node *dn = pdev->dev.of_node;

	data = kzalloc(sizeof(struct eeprom_data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	retval = sysfs_create_group(&pdev->dev.kobj, &sysfs_files);
	if (retval) {
		dev_err(&pdev->dev, "failed to create sysfs\n");
		kfree(data);
		return retval;
	}

	//  creating platform independent link under /sys/kernel/emperor/eeprom
	retval = sysfs_create_link(emperor_kobj, &pdev->dev.kobj, "eeprom");
	if (retval) {
		dev_err(&pdev->dev, "failed to link sysfs\n");
		kfree(data);
		return retval;
	}

	data->gpio_eeprom_wp = setup_gpio_pin(dn, "eeprom_wp", (GPIOF_DIR_OUT | GPIOF_EXPORT));
	if (data->gpio_eeprom_wp < 0) {
		dev_err(&pdev->dev, "failed to create eeprom_wp\n");
		kfree(data);
		return -ENOMEM;
	}

	platform_set_drvdata(pdev, data);

	pr_info("%s: driver successfully probed\n", DRIVER_NAME);

	return retval;
}

static int emperor_eeprom_remove(struct platform_device *pdev)
{
	struct eeprom_data *data;

	data = platform_get_drvdata(pdev);
	gpio_free(data->gpio_eeprom_wp);
	platform_set_drvdata(pdev, NULL);
	kfree(data);

	sysfs_remove_group(&pdev->dev.kobj, &sysfs_files);

	pr_info("%s: driver successfully removed\n", DRIVER_NAME);

	return 0;
}

static struct platform_driver emperor_eeprom_driver = {
	.driver		= {
		.name	= "emperor-eeprom",
		.owner	= THIS_MODULE,
		.of_match_table = emperor_eeprom_id_table,
	},
	.probe		= emperor_eeprom_probe,
	.remove		= emperor_eeprom_remove,
};

module_platform_driver(emperor_eeprom_driver);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Wim De Witte <de.witte.wim@gmail.com>");
